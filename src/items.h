#ifndef __ITEMS__
#define __ITEMS__

#include <vector>
#include <SFML/Graphics.hpp>

// item categories
#define ICAT_default 0
#define ICAT_potion 1

#define ITEM_IMGSIZE 64

#define COIN_TYPE_COUNT 2

class StattedCharacter;

enum pickupableType {
	ptItem, ptWeapon, ptCoin
};

enum itemEffectType {
	// Prefixing these with "ie" because apparently there was some namespace pollution before?
	ieDefaultEffect, ieHeal, ieSpirit, ieDamage, ieCure, ieStatusEffect, ieTerrain, ieAtmosphere, ieSpellLike, ieEquipment, ieMisc, ieReusableDamage
};

enum coinType {
	ctZinc, ctSilver
};

namespace items {
	const std::vector<std::string> coinTypeNames = {
		" zinc",
		" silver"
	};
}

class ItemOrWeapon {
	public:
		sf::Sprite* getSprite() const { return sprite; }
		virtual const std::string getName() const { return name; }
		const std::string getFlavor() const { return flavor; }
		virtual pickupableType getPickupableType() const = 0;
		virtual const std::string serialize() const = 0;
		virtual int getID() const = 0;
		virtual bool isPermanent() const = 0;

		sf::Sprite* sprite;
		std::string name;
		std::string flavor;
};

struct itemEffect {
	itemEffectType effect;
	int* params;
	//int param1;
	//int param2;
};


class item : public ItemOrWeapon {
	public:
		itemEffect useEffect;
		itemEffect throwEffect;
		const int id;
		int category;
		pickupableType getPickupableType() const override { return ptItem; }
		int getID() const override { return id; }
		const std::string serialize() const override { return "i " + std::to_string(id); }
		bool isPermanent() const override;
		
		item(int _id) : id(_id) {}
};

class coinPile : public ItemOrWeapon {
	public:
		int getCount() const { return count; }
		int getID() const override { return (int)type; }
		coinType getCoinType() const { return type; }
		pickupableType getPickupableType() const override { return ptCoin; }
		const std::string serialize() const override { return "$ " + std::to_string(type) + " " + std::to_string(count); }
		const std::string getName() const {
			return std::to_string(count) + items::coinTypeNames[type] + " coins";
		}
		bool isPermanent() const override { return type != ctZinc; }
		
		coinPile(coinType _ct, int _count);

	private:
		coinType type;
		int count;
};

namespace items {
	void init();
	item* getItem(int);
	
	void throwItemAtLocation(item*, int,int);
	bool useItemOnCharacter(itemEffect, StattedCharacter*);
	
	int itemCategoryToSpriteIndex(int);
}

#endif
