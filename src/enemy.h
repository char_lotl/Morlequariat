#ifndef ENEMY_H
#define ENEMY_H

#include "characters.h"


struct ConditionClause {
	char typeId;
	int* params;
};

struct BehaviorConditions {
	std::vector<ConditionClause> conditions;
	int behaviorIndex;
	
};

class MovementClause {
	public:
		virtual std::vector<std::pair<int, int>> filterPoints(const std::vector<std::pair<int, int>>& points) const = 0;
};

class MovementConstraint : public MovementClause {
	public:
		std::vector<std::pair<int, int>> filterPoints(const std::vector<std::pair<int, int>>& points) const;
		virtual bool validatePoint(const std::pair<int,int>&) const = 0;
};

class MovementOptimization : public MovementClause {
	public:
		std::vector<std::pair<int, int>> filterPoints(const std::vector<std::pair<int, int>>& points) const;
		virtual int calculateMetric(const std::pair<int,int>&) const = 0;
};

/*class DestinationClause : public MovementClause{
	public:
		std::vector<std::pair<int, int>> filterPoints(const std::vector<std::pair<int, int>>& points) const;
	private:
		char type;
		virtual std::vector<std::pair<int, int>> listDestinations();
};*/

struct ActionClause {
	char targetType;
	int* targetParams;
	char effectType;
	int* effectParams;
};

struct StaticPathfinding {
	int speed;
	bool allowPit;
	bool allowWater;
	bool allowLava;
	bool allowWall;
	std::vector<int> forbid;
};

struct Behavior {
	StaticPathfinding staticPathfinding;
	
	std::vector<MovementClause*> pathfinding;
	std::vector<ActionClause> actions;
};


class Enemy : public StattedCharacter {

	public:
		Enemy(int,int,int, ItemOrWeapon* drop = NULL);
		void takeStep();
		void takePathStep(int);
		int damageLocation(int x, int y, int amt, damage_type dtype);
		bool isDone() const {return hp<=0 || done;};
		void onDeath() override;
		void moveTo(int, int, bool intermediate=false) override;
		void setDone() {done = true;}

		std::string getName() const {return name;}
		int getAttackStrength() const {return attackStrength;}
		damage_type getAttackType() const {return attackType;}
		int getType() const {return typeId;}
		int getRange() const {return range;}
		ItemOrWeapon* getDrop() const {return drop;}
		bool hasFlags(int f) const {return (flags & f) == f;}
	
		void specificBeginTurn() override;
		void specificAddCondition(statusCondition& sc) override;
		bool specificConditionForTurn(statusCondition& sc) override;
		bool specificRemoveCondition(statusCondition& sc) override;
		
		bool isTouchingGround() const override;

		int getSpawnX() { return spawnX; }
		int getSpawnY() { return spawnY; }
		
	private:
		static sf::Texture* tex;
		
		bool hasAttacked;
		int stepsLeft;
		bool done;
		
		std::string name;
		
		int spawnX;
		int spawnY;
		
		ItemOrWeapon* drop;
		
		int attackStrength;
		damage_type attackType;
		
		int range;
		
		int typeId;
		
		int flags;
		
		int state;
		
		std::vector<std::pair<int,int>> path;
		int*** traversibilityMap;
		
		void calculateTraversibilityMap(int);
		bool canStep(int, int, int, int, const StaticPathfinding&) const;
		bool terrainAcceptable(int, int, int, int, const StaticPathfinding&) const;
		bool blockedOrOccupied(int, int) const;
		bool isReachable(int, int, int);
		bool conditionMet(const ConditionClause&, int);
		bool conditionsMet(const BehaviorConditions&);
		void chooseDestination(int);
		int takeAction(int);
		void clearMovementCache();
};

#endif
