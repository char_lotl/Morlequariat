#include "core.h"
#include "animations.h"
#include "timekeeping.h"
//#include "display.h"

#include <memory>

#define SPARKLEFRAMES 11

namespace display {
	void addProjectile(std::shared_ptr<animations::Projectile>);
}

namespace animations {

	sf::Texture damageTexture;
	sf::Texture waterTexture;
	sf::Texture thrownItemTexture;
	
	void init(){
		// initialize all standard animations here
		
		damageTexture.loadFromFile("img/damage.png");
		waterTexture.loadFromFile("img/water.png");
		thrownItemTexture.loadFromFile("img/item_overworld.png");
		
		for(int i=0; i<NUM_DAMAGE_TYPES; ++i){
			// This could easily be statically allocated. But these reusable animations are going to get mixed in with single-use dynamically-allocated ones during rendering.
			takeDamage[i] = std::shared_ptr<animation> { new animation };
			takeDamage[i]->frames = 5;
			takeDamage[i]->loop = false;
			takeDamage[i]->texture = &damageTexture;
			takeDamage[i]->frameRects = new sf::IntRect[takeDamage[i]->frames];
			takeDamage[i]->speed = 1;
			for(int j=0; j<takeDamage[i]->frames; ++j){
				takeDamage[i]->frameRects[j] = sf::IntRect(j*TILESIZE, i*TILESIZE, TILESIZE, TILESIZE);
			}
		}
		
		for(int i=0; i<4; ++i){
			flowingWater[i] = std::shared_ptr<animation> { new animation };
			flowingWater[i]->frames = 8;
			flowingWater[i]->loop = true;
			flowingWater[i]->texture = &waterTexture;
			flowingWater[i]->frameRects = new sf::IntRect[flowingWater[i]->frames];
			flowingWater[i]->speed = 4;
			for(int j=0; j<flowingWater[i]->frames; ++j){
				flowingWater[i]->frameRects[j] = sf::IntRect(j*TILESIZE, i*TILESIZE, TILESIZE, TILESIZE);
			}
		}
		
		
		airBuff.frames = 5;
		airBuff.loop = false;
		airBuff.texture = &damageTexture;
		airBuff.frameRects = new sf::IntRect[airBuff.frames];
		airBuff.speed = 1;
		for(int j=0; j<airBuff.frames; ++j){
			airBuff.frameRects[j] = sf::IntRect(j*TILESIZE, 7*TILESIZE, TILESIZE, TILESIZE);
		}
		
		sf::Texture* sparkleTexture = new sf::Texture;
		sparkleTexture->loadFromFile("img/sparkle.png");
		
		gainHealth = std::shared_ptr<animation> { new animation };
		gainHealth->frames = SPARKLEFRAMES;
		gainHealth->loop = false;
		gainHealth->speed = 1;
		gainHealth->texture = sparkleTexture;
		gainHealth->frameRects = new sf::IntRect[SPARKLEFRAMES];
		
		gainSpirit = std::shared_ptr<animation> { new animation };
		gainSpirit->frames = SPARKLEFRAMES;
		gainSpirit->loop = false;
		gainSpirit->speed = 2;
		gainSpirit->texture = sparkleTexture;
		gainSpirit->frameRects = new sf::IntRect[SPARKLEFRAMES];
		
		for(int i=0; i<SPARKLEFRAMES; ++i){
			gainHealth->frameRects[i] = sf::IntRect(0, 12*i, 101, 12);
			gainSpirit->frameRects[i] = sf::IntRect(101, 12*i, 101, 12);
		}
		
		
		sf::Texture* fountainTexture = new sf::Texture;
		fountainTexture->loadFromFile("img/fountain.png");
		fountain = std::shared_ptr<animation> { new animation };
		fountain->frames = 4;
		fountain->loop = true;
		fountain->texture = fountainTexture;
		fountain->frameRects = new sf::IntRect[fountain->frames];
		fountain->speed = 4;
		for(int i=0; i<fountain->frames; ++i){
			fountain->frameRects[i] = sf::IntRect(i*TILESIZE, 0, TILESIZE, TILESIZE);
		}
		
	}
		
	std::shared_ptr<animation> takeDamage[NUM_DAMAGE_TYPES]; // TODO add more damage types
	
	std::shared_ptr<animation> gainHealth;
	std::shared_ptr<animation> gainSpirit;
	
	std::shared_ptr<animation> fountain;
	std::shared_ptr<animation> flowingWater[4];
	
	animation airBuff;

	AnimationInstance::AnimationInstance(int x, int y, bool t, const std::shared_ptr<animations::animation>& _a) : offsetX(x), offsetY(y), atTile(t), a(_a) {
		sprite.setTexture(*a->texture);
		sprite.setPosition(x,y);
		start = timekeeping::currentTick();
	}

	sf::Sprite* AnimationInstance::update(){
		int now = timekeeping::currentTick();
		if((now - start)/a->speed >= a->frames) {
			if(!a->loop) return NULL;
			else start = now;
		}
		sf::IntRect ir = a->frameRects[(now - start)/a->speed];
		sprite.setTextureRect(ir);
		return &sprite; 
	}
	
	int animateProjectile(int startX, int startY, int endX, int endY, damage_type dt, std::function<void()> callback) {
		std::shared_ptr<Projectile> projectile {
			new Projectile(startX, startY, endX, endY, damageTexture, TILESIZE*5, TILESIZE*dt)
		};
		display::addProjectile(projectile);
		
		timekeeping::schedule(callback, projectile->getDuration());
		return projectile->getDuration();
	}
	
	int animateItemProjectile(int startX, int startY, int endX, int endY, int itemCategory, std::function<void()> callback) {
		std::shared_ptr<Projectile> projectile {
			new Projectile(startX, startY, endX, endY, thrownItemTexture,
				itemCategory%(thrownItemTexture.getSize().x/TILESIZE) * TILESIZE,
				itemCategory/(thrownItemTexture.getSize().x/TILESIZE) * TILESIZE)
		};
		display::addProjectile(projectile);
		
		timekeeping::schedule(callback, projectile->getDuration());
		return projectile->getDuration();
	}
	
	Projectile::Projectile(int startX, int startY, int endX, int endY, sf::Texture& tex, int textureX, int textureY)
			: sourceX(startX*TILESIZE), sourceY(startY*TILESIZE), destX(endX*TILESIZE), destY(endY*TILESIZE) {
		startTime = timekeeping::currentTick();
		
		int deltaX = endX - startX;
		if (deltaX < 0) deltaX *= -1;
		int deltaY = endY - startY;
		if (deltaY < 0) deltaY *= -1;
		duration = 3 * (deltaX > deltaY ? deltaX : deltaY);
		
		sprite.setTexture(tex);
		sprite.setTextureRect(sf::IntRect(textureX, textureY, TILESIZE, TILESIZE));
	}

	int Projectile::getCurrentX() const {
		return sourceX + (timekeeping::currentTick() - startTime) * (destX - sourceX) / duration;
	}
	
	int Projectile::getCurrentY() const {
		return sourceY + (timekeeping::currentTick() - startTime) * (destY - sourceY) / duration;
	}
	
	bool Projectile::isDone() const {
		return timekeeping::currentTick() - startTime > duration;
	}
}

