#include "triggers.h"
#include "world.h"
//#include "items.h"
//#include "animations.h"
#include "characters.h"
//#include "core.h"
#include "cutscenes.h"
#include "timekeeping.h"
#include "music.h"

#include <vector>
#include <sstream>
#include <iostream>

using namespace std;

class Enemy {
	public:
		int getType() const;
};

namespace triggers {

	vector<GeographicTrigger> levelTriggers;
	vector<AggregateTrigger*> metaTriggers;
	vector<ResetTrigger> resetTriggers;
	vector<TurnTrigger> turnTriggers;
	
	bool conditionTrue(const trigger_condition& c) {
		switch(c.type) {
			case tileImage: {
				return world::getTileImage(c.x, c.y) == c.id;
			}
			case tileTerrain: return world::getTerrain(c.x, c.y) == c.id;
			case player: {
				return characters::playable[c.id]->getX() == c.x &&
				       characters::playable[c.id]->getY() == c.y;
			}
			case enemy:
				auto enemy = world::getEnemyAt(c.x, c.y);
				return enemy && (enemy->getType() == c.id);
		}
		return false;
	}

	void doOutput(const trigger_output& o) {
		switch(o.type){
			case propagate:
				triggerMeta(o.params[0], o.params[1]);
				break;
				
			case delay: {
				int target = o.params[0];
				int source = o.params[1];
				timekeeping::schedule(
					[target, source](){
						triggers::triggerMeta(target, source);
					}, o.params[2]);
				break;
			}
			
			case decrement: {
				metaTriggers[o.params[0]]->decrement();
				break;
			}
			
			case reset: {
				metaTriggers[o.params[0]]->reset();
				break;
			}
			
			case spawnItem: {
				world::putItem(o.params[0], o.params[1], items::getItem(o.params[2]));
				break;
			}
			
			case spawnWeapon: {
				world::putItem(o.params[0], o.params[1], weapons::getWeapon(o.params[2]));
				break;
			}
			
			case damage: {
				StattedCharacter* target = world::getCharacterAt(o.params[0], o.params[1]);
				if(target) target->takeDamage(o.params[2], (damage_type)o.params[3]);
				break;
			}
			
			case terrain: {
				world::setTerrain(o.params[0], o.params[1], o.params[2]);
				break;
			}
			
			case setTile: {
				world::setTileImage(o.params[0], o.params[1], o.params[2]);
				break;
			}
			
			case cutscene: {
				cutscenes::startDialogue(o.params[0]);
				break;
			}
			
			case flags: {
				world::setRoomFlags(o.params[0]);
				break;
			}
			
			case music: {
				music::switchTo(o.params[0]);
				break;
			}
			
			case sound: {
				music::playSound(std::to_string(o.params[0]));
				break;
			}
			
			default: cout << "unexpected trigger type " << o.type << endl;
				break;
		}
	}

	void Trigger::execute(){
		for(int i=0; i<conditions.size(); ++i) {
			if(!conditionTrue(conditions[i])) {
				if (elseIndex >= 0) triggerMeta(elseIndex, 0);
				return;
			}
		}
		for(int i=0; i<output.size(); ++i){
			doOutput(output[i]);
		}
	}
	
	void Trigger::addOutput(trigger_output& o){
		output.push_back(o);
	}
	
	GeographicTrigger::GeographicTrigger(input_type _type, int _param, int _x1, int _x2, int _y1, int _y2) : type(_type), param(_param), x1(_x1), x2(_x2), y1(_y1), y2(_y2){}
		
	bool GeographicTrigger::containsPoint(int x, int y) {
		return x >= x1 && x <= x2 && y >= y1 && y <= y2;
	}
	
	bool GeographicTrigger::appropriateParam(int p) {
		return (param == -1) || (p == param) || (type == damageAmount && p > param);
	}

	void JunctionTrigger::takeInput(int source){
		execute();
	}

	void ThresholdTrigger::takeInput(int source){
		++state;
		if(state == threshold) execute();
	}
	
	void ThresholdTrigger::decrement(){
		if(state == threshold) for(int i=0;i<resetTriggers.size();++i) if(resetTriggers[i].listenTo == id) resetTriggers[i].execute();
		if(state>0) --state;
	}
	
	void ThresholdTrigger::reset(){
		if(state >= threshold) for(int i=0;i<resetTriggers.size();++i) if(resetTriggers[i].listenTo == id) resetTriggers[i].execute();
		state = 0;
	}

	void SequenceTrigger::takeInput(int source){
		if(source == sequence[state]){
			++state;
			if(state >= sequence.size()) execute();
		}
	}

	void ResettingSequenceTrigger::takeInput(int source){
		if(source == sequence[state]){
			SequenceTrigger::takeInput(source);
		} else {
			state = 0;
			for(int i=0;i<resetTriggers.size();++i) if(resetTriggers[i].listenTo == id) resetTriggers[i].execute();
		}
	}
	
	input_type charToType(char c){
		switch(c){
			case 'c': return charEnter;
			case 'C': return charExit;
			case 'i': return itemPut;
			case 'I': return itemTake;
			case 'e': return enemyEnter;
			case 'E': return enemyExit;
			case 'd': return damageType;
			case 'D': return damageAmount;
			case 'k': return enemyDie;
			case 'u': return useItem;
		}
	}
	
	void parseConditions(std::stringstream& ss, std::vector<trigger_condition>& conditions) {
		while(ss.rdbuf()->in_avail()){
			trigger_condition condition;
			
			char typeChar;
			ss >> typeChar;
			
			condition_type type;
			switch(typeChar) {
				case ']': return;
				case 'c': condition.type = player; break;
				case 'e': condition.type = enemy; break;
				case 'i': condition.type = item; break;
				case 'T': condition.type = tileImage; break;
				case 't': condition.type = tileTerrain; break;
			}
			
			ss >> condition.x >> condition.y >> condition.id;
			
			conditions.push_back(condition);
			
		}
		std::cerr << "fatal syntax error: unclosed bracket in trigger condition" << std::endl;
	}

	void parseTrigger(std::stringstream& ss){
		
		char typeChar;
		ss >> typeChar;
		
		bool aggregate = false;
		
		bool areaTrigger = false;
		
		Trigger loadTrigger;
		
		std::vector<trigger_condition> conditions;
		
		int elseIndex = -1;
		
		if (typeChar == '[') {
			parseConditions(ss, conditions);
			ss >> typeChar;
			
			if (typeChar == '|') {
				ss >> elseIndex >> typeChar;
			}
		}
		
		switch(typeChar){
			case 'a':
				areaTrigger = true;
				ss >> typeChar;
				// FALLTHRU
			case 'k':
			case 'c':
			case 'C':
			case 'i':
			case 'I':
			case 'e':
			case 'E':
			case 'd':
			case 'D':
			case 'u': {
				
				int x1, x2, y1, y2, param;
				ss >> x1 >> y1;
				
				if(areaTrigger) {
					ss >> x2 >> y2;
				} else {
					x2 = x1;
					y2 = y1;
				}
				
				ss >> param;
				
				levelTriggers.push_back(GeographicTrigger(charToType(typeChar), param, x1, x2, y1, y2));
				break;
			}
			case 'J': {
				metaTriggers.push_back(new JunctionTrigger(metaTriggers.size()));
				aggregate = true;
				break;
			}
			
			case 'T': {
				int param;
				ss >> param;
				metaTriggers.push_back(new ThresholdTrigger(metaTriggers.size(), param));
				aggregate = true;
				break;
			}
			
			case 'S': {
				int count;
				ss >> count;
				vector<int> sequence;
				
				int item;
				for(int i=0; i<count; ++i){
					ss >> item;
					sequence.push_back(item);
				}

				metaTriggers.push_back(new SequenceTrigger(metaTriggers.size(), sequence));
				aggregate = true;
				break;
			}
			
			case 'R': {
				int id;
				ss >> id;
				resetTriggers.push_back(ResetTrigger(id));
				break;
			}
			
			case 'B': {
				turnTriggers.push_back(TurnTrigger());
				break;
			}
			
			case 'L': {
				// on-load trigger. don't need to store this anywhere
			}
		}
			
		if(aggregate) {
			metaTriggers[metaTriggers.size()-1]->setConditions(conditions);
			metaTriggers[metaTriggers.size()-1]->setElseIndex(elseIndex);
		}
		else if(typeChar == 'R') {
			resetTriggers[resetTriggers.size()-1].setConditions(conditions);
			resetTriggers[resetTriggers.size()-1].setElseIndex(elseIndex);
		}
		else if(typeChar == 'B') {
			turnTriggers[turnTriggers.size()-1].setConditions(conditions);
			turnTriggers[turnTriggers.size()-1].setElseIndex(elseIndex);
		}
		else if(typeChar != 'L') {
			levelTriggers[levelTriggers.size()-1].setConditions(conditions);
			levelTriggers[levelTriggers.size()-1].setElseIndex(elseIndex);
		}
		
		while(ss.rdbuf()->in_avail()){
			trigger_output output;
			
			int params;
			char outputChar;
			ss >> outputChar;
			switch(outputChar){
				case 'P': params = 2; output.type = output_type::propagate; break;
				case 'D': params = 3; output.type = output_type::delay; break;
				case 'M': params = 1; output.type = output_type::decrement; break;
				case 'R': params = 1; output.type = output_type::reset; break;
				case 't': params = 3; output.type = output_type::terrain; break;
				case 'T': params = 3; output.type = output_type::setTile; break;
				case 'e': params = 3; output.type = output_type::spawnEnemy; break;
				case 'i': params = 3; output.type = output_type::spawnItem; break;
				case 'w': params = 3; output.type = output_type::spawnWeapon; break;
				case 's': params = 1; output.type = output_type::sound; break;
				case 'm': params = 1; output.type = output_type::music; break;
				case 'a': params = 4; output.type = output_type::animation; break;
				case 'd': params = 4; output.type = output_type::damage; break;
				case 'c': params = 1; output.type = output_type::cutscene; break;
				case 'f': params = 1; output.type = output_type::flags; break;
				default: std::cout << "Unknown trigger output type " << outputChar << " in \"" << ss.str() << "\"" << std::endl;
				// TODO validation and print about it
			}
			
			output.params = new int[params];
			for(int i=0; i<params;++i) ss >> output.params[i];
			
			if(aggregate) {
				metaTriggers[metaTriggers.size()-1]->addOutput(output);
			} else if(typeChar == 'R') {
				resetTriggers[resetTriggers.size()-1].addOutput(output);
			} else if(typeChar == 'L') {
				doOutput(output);
			} else if(typeChar == 'B') {
				turnTriggers[turnTriggers.size()-1].addOutput(output);
			} else {
				levelTriggers[levelTriggers.size()-1].addOutput(output);
			}
		}
	}
	
	void clear(){
		levelTriggers.clear();
		for(int i=0;i<metaTriggers.size();++i) delete metaTriggers[i];
		metaTriggers.clear();
		resetTriggers.clear();
		turnTriggers.clear();
	}
	
	void triggerMeta(int index, int parameter){
		metaTriggers[index]->takeInput(parameter);
	}
	
	void initializeCachedMetaTrigger(int index, int value) {
		if(index >= metaTriggers.size()) return;
		metaTriggers[index]->setState(value);
	}
	
	const std::vector<int> listCachedMetaTriggers() {
		std::vector<int> states;
		for(int i=0; i<metaTriggers.size(); ++i) states.push_back(metaTriggers[i]->getState());
		return states;
	}
	
	void processGeographicTrigger(input_type type, int x, int y, int param){
		for(int i=0; i<levelTriggers.size(); ++i){
			GeographicTrigger t = levelTriggers[i];
			if(t.type == type && t.containsPoint(x,y) && t.appropriateParam(param)) t.execute();
		}
	}
	void processPerimeterTrigger(input_type type, int inX, int inY, int outX, int outY, int param) {
		for(int i=0; i<levelTriggers.size(); ++i){
			GeographicTrigger t = levelTriggers[i];
			if(t.type == type && t.containsPoint(inX,inY) && !t.containsPoint(outX, outY) && t.appropriateParam(param)) t.execute();
		}
	}
	void processTurnStartTriggers() {
		for(TurnTrigger t : turnTriggers) t.execute();
	}

}
