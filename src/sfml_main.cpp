
#include <thread>
#include <string>
#include <iostream>
#include <fstream>

#include "status.h"
#include "buttons.h"
#include "display.h"
#include "characters.h"
#include "world.h"
#include "animations.h"
#include "triggers.h"
#include "cutscenes.h"
#include "spells.h"
#include "music.h"
#include "timekeeping.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#ifdef NATIVE_THREADS
#include <thread>
#endif

using namespace std;

namespace cutscenes {
//	bool hasDialogue();
}

void resize(sf::RenderWindow& window, int w, int h) {
	window.setView(sf::View(sf::FloatRect(0,0,w,h)));
	
	display::setGlobalScale(w, h);
}

void loadSettings() {
	ifstream settingsSave("save/settings");
	int music, sound, ui;
	settingsSave >> music >> sound >> ui;
	music::setMusicVolume(music);
	music::setSoundVolume(sound);
	buttons::setUiTheme(ui);
}

void saveSettings() {
	ofstream settingsSave("save/settings");
	settingsSave << music::getMusicVolume() << " " << music::getSoundVolume() << " " << buttons::getUiTheme() << endl;
}

int main(){

	cout << "START" << endl;
	items::init();
	cout << "Initialized Items" << endl;
	weapons::init();
	cout << "Initialized Weapons" << endl;
	spells::init();
	cout << "Initialized Spells" << endl;
	display::init();
	cout << "Initialized Display" << endl;
	animations::init();
	cout << "Initialized Animations" << endl;
	characters::init();
	cout << "Initialized Characters" << endl;
	buttons::init();
	cout << "Initialized User Interface" << endl;
	cutscenes::init();

	sf::Image icon;
	icon.loadFromFile("img/morlico.png");
	cout << "main: loaded icon" << endl;
	
	loadSettings();
	
	sf::RenderWindow window(sf::VideoMode(TOTALWIDTH, TOTALHEIGHT), "MORLEQUARIAT");
	window.setMouseCursorVisible(false);
	  
	//TODO screensize * tilesize + menusize
	cout << "main: created window" << endl;
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	cout << "main: set icon" << endl;

#ifdef NATIVE_THREAD
	std::thread clockThread(timekeeping::clock);
#else
	sf::Thread clockThread(&timekeeping::clock);
	clockThread.launch();
#endif

	buttons::showTitleScreen();

	sf::Event event;
	while(buttons::gameOpen){
		
		//triggers::processTurn();
		if(!display::isStartMenu()) world::checkPlayerTurn(); // Check if the enemies just finished moving //TODO THIS SHOULD NOT BE EVERY FRAME!

		while (window.pollEvent(event)) { // Might be beneficial to have an event process thread and a drawing/control thread.
			const unordered_set<Button*>& visibleButtons = buttons::listButtons();
		
			switch(event.type) {
				
				case sf::Event::Resized: {
					if (event.size.width < TOTALWIDTH || event.size.height < TOTALHEIGHT) {
						window.setSize(sf::Vector2u(TOTALWIDTH, TOTALHEIGHT));
					} else {
						resize(window, event.size.width, event.size.height);
					}
					break;
				}
	
				case sf::Event::Closed: {
					// Trigger some sort of confirmation, actually. But that can be handled later.
					buttons::gameOpen = false;
					break;
				}

				case sf::Event::MouseMoved: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseMove.x / display::getGlobalScale(), event.mouseMove.y / display::getGlobalScale())){
							(*itr)->highlight();
						} else {
							(*itr)->reset();
						}
					}
					display::updateMouse(event.mouseMove.x, event.mouseMove.y);
					break;
				}
				
				case sf::Event::MouseButtonPressed: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseButton.x / display::getGlobalScale(), event.mouseButton.y / display::getGlobalScale())){
							(*itr)->depress();
						} else {
							(*itr)->reset();
						}
					}
					break;
				}
				
				case sf::Event::MouseButtonReleased: {
					if(event.mouseButton.button == sf::Mouse::Button::Left){
						int mouseX = event.mouseButton.x / display::getGlobalScale();
						int mouseY = event.mouseButton.y / display::getGlobalScale();
						
						bool handledClick = false;
						Button* currentButton;
						for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
							currentButton = *itr;
							if(currentButton->rect.contains(mouseX, mouseY) && !currentButton->isDisabled()){
								currentButton->effect();
								currentButton->reset();
								currentButton->highlight();
								handledClick = true;
								break;
							}
						}
						if(!handledClick && display::hasDialogue()) {
							handledClick = true;
							if(cutscenes::isAdvanceable()) {
								cutscenes::advanceDialogue();
							} else {
								display::skipDialogue();
							}
						}
						if(!handledClick && display::isClearView()) for(int i=0; i<characters::playable.size(); ++i){
							if(characters::playable[i]->getRoomId() == world::getRoomId() && characters::playable[i]->getSprite()->getGlobalBounds().contains(mouseX, mouseY)){
								std::cout << "Global bounds of character sprite: left "
									<< characters::playable[i]->getSprite()->getGlobalBounds().left <<
									", top " << characters::playable[i]->getSprite()->getGlobalBounds().top <<
									", width " << characters::playable[i]->getSprite()->getGlobalBounds().width <<
									", height " << characters::playable[i]->getSprite()->getGlobalBounds().height <<
									", mouse x " << mouseX <<
									", mouse y " << mouseY << std::endl;
									
								buttons::selectCharacter(i);
								handledClick = true;
								break;
							}
						}
						if(!handledClick && display::isLookInterface()) {
							buttons::selectLook();
							handledClick = true;
							break;
						}
						if(!handledClick && display::isClearView()) for(int i=0; i<world::getNPCs().size(); ++i){
							if(world::getNPCs()[i]->getSprite()->getGlobalBounds().contains(mouseX, mouseY)){
								world::getNPCs()[i]->startDialogue();
								handledClick = true;
								break;
							}
						}
					}
					break;
				}
				
				case sf::Event::KeyPressed: {
					switch(event.key.code){
						case sf::Keyboard::Num1:
						case sf::Keyboard::Num2:
						case sf::Keyboard::Num3:
						case sf::Keyboard::Num4:
						case sf::Keyboard::Num5:
						case sf::Keyboard::Num6:
						case sf::Keyboard::Num7:  buttons::onNumberKey(event.key.code - sf::Keyboard::Num1); break;
						
						case sf::Keyboard::Space: if (cutscenes::hasDialogue()) display::skipDialogue(); else buttons::onSpacebar(); break;
						
						case sf::Keyboard::A:
						case sf::Keyboard::Left:  buttons::onArrowKey(direction::_left); break;
						case sf::Keyboard::D:
						case sf::Keyboard::Right: buttons::onArrowKey(direction::_right); break;
						case sf::Keyboard::W:
						case sf::Keyboard::Up:    buttons::onArrowKey(direction::_up); break;
						case sf::Keyboard::S:
						case sf::Keyboard::Down:  buttons::onArrowKey(direction::_down); break;
						
						case sf::Keyboard::Q:     if (cutscenes::hasDialogue()) display::skipDialogue(); else buttons::onItemHotkey(); break;
						case sf::Keyboard::E:     buttons::onAffirmativeKey(); break;
						case sf::Keyboard::R:     buttons::onStatus(); break;
						case sf::Keyboard::F:     buttons::onLook(); break;
						
						case sf::Keyboard::Tab:   buttons::onWait(); break;
						case sf::Keyboard::Escape:buttons::onCancel(); break;
						case sf::Keyboard::C:     buttons::onNextCharacter(); break;
						//case sf::Keyboard::Z:     buttons::onAffirmativeKey(); break;
						//case sf::Keyboard::X: display::skipDialogue(); break;
						
						case sf::Keyboard::F5:
							buttons::setAnimationBlocking(false); // debug feature for if the controls get screwed up
							break;
							
						case sf::Keyboard::F11:
							display::toggleFullscreen();
							if(display::isFullscreen()) {
								sf::VideoMode fsmode = sf::VideoMode::getFullscreenModes()[0];
								window.create(fsmode, "MORLEQUARIAT", sf::Style::Fullscreen);
								resize(window, fsmode.width, fsmode.height);
							} else {
								window.create(sf::VideoMode(TOTALWIDTH, TOTALHEIGHT), "MORLEQUARIAT");
								resize(window, TOTALWIDTH, TOTALHEIGHT);
							}
							break;
					}
				}
			}
		}
		
		if (display::isStartMenu()) {
			display::drawTitleScreen(window);
		} else {
			display::drawWindow(window);
		}
		
	}
	
	timekeeping::stop();

#ifdef NATIVE_THREAD
	clockThread.join();
#else
	clockThread.wait();
#endif

	//world::freeMap();
	display::cleanup();
	music::cleanup();
	//for(int i=0; i<tilesetSize; ++i) delete tileset[i];
	//delete[] tileset;
	
	window.close();
	
	saveSettings();

	return 0;
}
