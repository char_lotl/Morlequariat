#ifndef CORE
#define CORE

extern const int TILESIZE;

enum direction { _left, _up, _right, _down };

#define NUM_DAMAGE_TYPES 6
enum damage_type { physical, holy, fire, frost, poison, corrupt };

enum range_type { self, melee, radius, linear, long_melee, radius_line, custom };

#endif
