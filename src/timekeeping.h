#ifndef __TIMEKEEPING__
#define __TIMEKEEPING__

#include <functional>

namespace timekeeping {
	
	int currentTick();
	void stop();
	
	void schedule(std::function<void()> f, int delay);
	
	void clock();
	
}



#endif
