#ifndef SAVEFILE_H
#define SAVEFILE_H


namespace savefile {
	
	void save(int);
	void load(int);
	void clear(int);
	
	bool exists(int);
	
	
}

#endif