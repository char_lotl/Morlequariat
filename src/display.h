
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include <list>
#include <memory>

#define SCREENWIDTH (TILESIZE * 16)
#define SCREENHEIGHT (TILESIZE/2 * 19)

#define TOTALWIDTH (SCREENWIDTH+128) // 896
#define TOTALHEIGHT (SCREENHEIGHT+64) // 544

// 1024 x 768 is apparently common

/* 
#define TOTALWIDTH 840
#define TOTALHEIGHT 525

#define SCREENWIDTH (TOTALWIDTH - 128)
#define SCREENHEIGHT (TOTALHEIGHT - 64)
*/

#define SECTIONPAD 8

class Character;
class PlayerCharacter;
class Enemy;
namespace animations { 
	struct animation;
	class Projectile;
}

extern int globalScale;

namespace display {

	void scheduleSetTile(int x, int y, int t);
	void loadBackground(unsigned char** imgIds, unsigned char** typeIds, int width, int height, int tileset);
	
	void drawWindow(sf::RenderWindow& window);
	void drawTitleScreen(sf::RenderWindow& window);
	
	void setGlobalScale(int, int);
	int getGlobalScale();
	void toggleFullscreen();
	bool isFullscreen();
	void updateMouse(int,int);
	
	void setUiTheme(int);
	
	void clearView();
	void showInventory();
	void showSpellList();
	void showSpellDescription();
	void showGroundItems();
	void showShopItems();
	void showCharacterStatus();
	void showDeathScreen();
	void showStartMenu();
	void showLookInterface();
	bool isClearView();
	bool isStartMenu();
	bool isLookInterface();
	bool isDeathScreen();
	
	void addAnimation(int, int, const std::shared_ptr<animations::animation>&);
	void addAnimationAtTile(int, int, const std::shared_ptr<animations::animation>&, bool foreground=true);
	void removeAnimationAtTile(int, int, bool foreground=true);
	void clearAnimations();
	void showDamageText(int, int, int);
	void addProjectile(std::shared_ptr<animations::Projectile>);
	
	void sparkleHealthMeter(PlayerCharacter*);
	void sparkleSpiritMeter(PlayerCharacter*);
	
	void center(int x, int y); // centers on the (x,y)th tile of the grid
	void center(Character* character);

	void showDialogue(const std::string&, const std::string&, int, int voiceid = -1);
	void showDialogueOptions(const std::shared_ptr<std::vector<std::string> >&);
	void skipDialogue();
	void hideDialogue();
	bool hasDialogue();
	
	void showSceneImage(int);

	void showEnemyStatus(Enemy*);

	void init();
	void cleanup();

}
