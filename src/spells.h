#ifndef __SPELLS__
#define __SPELLS__

#include <memory>
#include <vector>

#include "core.h"

struct spell {
	int id;
	int cost;
	int rangeMod;
	range_type rtype;
	std::string name;
	std::string description;
};

class PlayerCharacter; // forward definition

namespace spells {
	void init();
	std::vector<std::pair<int, int> > getTargets(std::shared_ptr<spell> s, PlayerCharacter* caster);
	int getAreaRadius(std::shared_ptr<spell> s, PlayerCharacter* caster);
	std::shared_ptr<spell> get(int);
}

#endif
