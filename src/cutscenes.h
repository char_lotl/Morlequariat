#ifndef __CUTSCENES__
#define __CUTSCENES__

#include <string>
#include <memory>
#include <variant>

class NPC;

namespace cutscenes {
	
	struct dialogueLine {
		int majorCharacter;
		int minorCharacter;
		std::string text;
	};
	
	struct dialogueBranch {
		int count;
		dialogueLine prompt;
		std::shared_ptr<std::vector<int> > indices;
		std::shared_ptr<std::vector<std::string>> textOptions;
	};
	
	
	struct cutsceneEvent {
		char type;
		
		std::variant<dialogueLine, dialogueBranch, std::shared_ptr<std::vector<int> > > details;
		
		const dialogueLine& getLine() const { return std::get<dialogueLine>(details); }
		const dialogueBranch& getBranch() const { return std::get<dialogueBranch>(details); }
		const std::shared_ptr<std::vector<int> >& getParams() const { return std::get<std::shared_ptr<std::vector<int> > >(details); }
	};
	
	void init();
	void loadDialogueFile(int);
	void loadRoomDialogue(int);
	const std::vector<cutsceneEvent> loadNpcItemDialogue(int);
	void startDialogue(int startIndex = 0);
	void startDialogueFromBranch(int);
	void startDialogueForItem(std::shared_ptr<NPC>, int);
	void advanceDialogue();
	
	void cancelDialogue();
	
	void setAdvanceable();
	bool isAdvanceable();
	
	int getResponseCount();
	bool hasDialogue();

	int getFullscreenR();
	int getFullscreenG();
	int getFullscreenB();
}

#endif
