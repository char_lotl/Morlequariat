#include "characters.h"
#include "terrain.h"
#include "animations.h"
#include "display.h"
#include "timekeeping.h"
#include "music.h"
#include <iostream>
#include <sstream>

//forward declaration
namespace cutscenes {
	void startDialogue(int);
}

namespace world {
	void handleTransientTerrain(unsigned char, StattedCharacter*);
	void damageAtTile(int, int, int, damage_type);
}

namespace buttons {
	void setAnimationBlocking(bool);
}

void StattedCharacter::beginTurn(){
	
	world::handleTransientTerrain(world::getTransientTerrain(x,y), this);
	// Durations of status conditions tick down.
	// Remove any that are expiring.
	std::cout << "There are " << statusEffects.size() << " status effects." << std::endl;
	int i = 0;
	while(i < statusEffects.size()){
		std::cout << "Checking " << std::to_string(i) << std::endl;
		if(!specificConditionForTurn(statusEffects[i])) conditionForTurn(statusEffects[i]);
		if(statusEffects[i].intensity == 0) {
			statusEffects.erase(statusEffects.begin() + i);
			std::cout << "Erased" << std::endl;
		} else {
			++i;
			std::cout << "Skipped" << std::endl;
		}
	}
}

int StattedCharacter::getDamageTaken(damage_type dt) const {
	if (dt != holy && hasStatusEffect(conditionId::barrier)) {
		return 0;
	}
	int percent = damageTaken[dt];
	if (dt == fire && hasStatusEffect(conditionId::phoenix)) {
		percent = -100;
	}
	percent += hasStatusEffect(conditionId::phDefDn + dt);
	percent -= hasStatusEffect(conditionId::phDefUp + dt);
	
	return percent;
}

int StattedCharacter::takeDamage(int amt, damage_type dt){
	int delta = (amt * getDamageTaken(dt)) / 100;

	hp -= delta;
	if (x >= 0 || y >= 0) {
		// don't show damage text for offscreen players in travel mode
		display::showDamageText(delta, x,y/*, (animations::takeDamage+dt)->frames*3*/);
	}
	if(hp <= 0) {
		delta += hp;
		hp = 0;
		onDeath();
	}
	if(hp > hpMax) {
		delta -= (hp - hpMax);
		hp = hpMax;
	}
	
	if(delta < 0) {
		music::playSound("9");
	} else if(delta == 0) {
		music::playSound("8");
	} else {
		music::playSound(std::to_string(10 + dt));
	}
	
	return delta;
}

int StattedCharacter::hasStatusEffect(int id) const {
	for(int i=0; i<statusEffects.size(); ++i) {
		if(statusEffects[i].id == id) return statusEffects[i].intensity > 0 ? statusEffects[i].intensity : true;
	}
	return false;
}

int StattedCharacter::totalStatusEffectIntensity(int id) const {
	int total = 0;
	for(int i=0; i<statusEffects.size(); ++i) {
		if(statusEffects[i].id == id) total += statusEffects[i].intensity;
	}
	return total;
}

		
void StattedCharacter::heal(int h) {
	display::showDamageText(-h, getX(), getY());
	if(hp+h>hpMax) hp=hpMax; else hp+=h;
}

void StattedCharacter::addCondition(conditionId id, int intensity) {
	
	switch(id){
		case poisoned:
			display::addAnimationAtTile(x,y, animations::takeDamage[damage_type::poison] ); break;
		case spicy:
		case burning:
			display::addAnimationAtTile(x,y, animations::takeDamage[damage_type::fire] ); break;
		case minty:
			display::addAnimationAtTile(x,y, animations::takeDamage[damage_type::frost] ); break;
		//case levit:
			//display::addAnimationAtTile(x,y, std::shared_ptr<animations::animation> { &animations::airBuff } ); break;
		
		// Don't modify the percent damage taken stats here. Instead, calling getDamageTaken takes buffs and debuffs into account.
		
		//case spdDn: break;
		case spdUp: speed += intensity; break;
	}

	for(int i=0; i<statusEffects.size(); ++i) {
		if(statusEffects[i].id == id) {
			// negative duration denotes unlimited
			statusEffects[i].intensity += intensity;
			return;
		}
	}
	
	statusCondition s;
	s.id = id;
	s.intensity = intensity;
	specificAddCondition(s);
	statusEffects.push_back(s);
}

void StattedCharacter::conditionForTurn(statusCondition& sc){
	switch(sc.id){
		case poisoned:
			display::addAnimationAtTile(x, y, animations::takeDamage[damage_type::poison] );
			takeDamage(sc.intensity, damage_type::poison); break;
		case burning:
			display::addAnimationAtTile(x, y, animations::takeDamage[damage_type::fire] );
			takeDamage(sc.intensity, damage_type::fire); --sc.intensity; break;
		case cursed:
			display::addAnimationAtTile(x, y, animations::takeDamage[damage_type::corrupt] );
			takeDamage(sc.intensity, damage_type::corrupt); ++sc.intensity; break;
		case regen:     heal(sc.intensity); break;
		case phDefDn:   removeCondition(sc);
	}
}

void StattedCharacter::removeCondition(conditionId id){
	for(int i=0; i<statusEffects.size(); ++i) {
		if(statusEffects[i].id == id) {
			removeCondition(statusEffects[i]);
			break;
		}
	}
}

void StattedCharacter::removeCondition(statusCondition& sc){

	bool removed = specificRemoveCondition(sc);
	
	if(!removed) {

		switch(sc.id){
			/*case spdDn: //speed += sc.intensity; break;
			case phDefDn:
			case hlDefDn:
			case fiDefDn:
			case fsDefDn:
			case pnDefDn:
			case crDefDn:
				damageTaken[sc.id - conditionId::phDefDn] -= sc.intensity; break;
				
			case spdUp: speed -= sc.intensity; break;
			case phDefUp:
			case hlDefUp:
			case fiDefUp:
			case fsDefUp:
			case pnDefUp:
			case crDefUp:
				damageTaken[sc.id - conditionId::phDefUp] += sc.intensity; break;*/
		}
	}
	
	auto itr = statusEffects.begin();
	while(itr != statusEffects.end()){
		if(&*itr == &sc) statusEffects.erase(itr);
		else ++itr;
	}

}

void StattedCharacter::onPush(direction dir, int distance, int collisionDamage) {
	int newX = x;
	int newY = y;
	
	switch(dir) {
		case _up:    newY -= 1; break;
		case _down:  newY += 1; break;
		case _left:  newX -= 1; break;
		case _right: newX += 1; break;
	}
	
	if(world::blockedOrOccupied(newX, newY) || world::blockedOneWay(x, y, newX, newY)) {
		if (collisionDamage) {
			takeDamage(collisionDamage * distance, damage_type::physical);
			world::damageAtTile(newX, newY, collisionDamage * distance, damage_type::physical);
		}
		buttons::setAnimationBlocking(false);
	} else {
		--distance;
		if(distance) timekeeping::schedule([this, dir, distance, collisionDamage](){this->onPush(dir, distance, collisionDamage);}, 1);
		else buttons::setAnimationBlocking(false);
		moveTo(newX, newY, distance>0);
	}
}

characters::animation* characters::createLinearAnimation(int textureOffsetX, int textureOffsetY, int frames, int deltaX, int deltaY, int width, int height, animation* next, int durationMultiplier, int staticOffsetX, int staticOffsetY){
	animation* a = new animation;
	a->frames = frames;
	a->frameRects = new sf::IntRect[frames];
	a->offsets = new sf::Vector2i[frames];
	for(int i=0; i<frames;++i){
		a->frameRects[i] = sf::IntRect(textureOffsetX + i*width, textureOffsetY, width, height);
		a->offsets[i] = sf::Vector2i(deltaX*i/frames + staticOffsetX, deltaY*i/frames + staticOffsetY);
	}
	a->next = next ? next : a; // animations loop by default
	a->frameDuration = durationMultiplier;
	a->offsetCamera = false;
	return a;
}

void Character::updateAnimation(){
	int now = timekeeping::currentTick();
	int frameIndex = (now - animationStart) / currentAnimation->frameDuration;
	if(frameIndex >= currentAnimation->frames) {
		currentAnimation = currentAnimation->next;
		animationStart = now;
		frameIndex = 0;
	}
	sf::IntRect ir = currentAnimation->frameRects[frameIndex];
	sprite.setTextureRect(ir);
	sf::Vector2i offset = currentAnimation->offsets[frameIndex];
	pixelOffsetX = offset.x;
	pixelOffsetY = offset.y;
}

void Character::setAnimation(characters::animation* a, bool reset){
	if(!reset && currentAnimation == a) return; // don't reset the animation
	currentAnimation = a;
	animationStart = timekeeping::currentTick();
	sf::Vector2i offset = currentAnimation->offsets[0];
	pixelOffsetX = offset.x;
	pixelOffsetY = offset.y;

	
}

NPC::NPC(int _id) {
	
	id = _id;
	
	tex.loadFromFile("img/npc/"+std::to_string(_id)+".png");
	sprite.setTexture(tex);
	
	int idleFrames = 4;
	int frameDuration = 4 * FRAME_DURATION_DEFAULT;
	
	std::ifstream input("misc/npc/" + std::to_string(_id));
	if(!input.good()) {
		std::cout << "can't load npc file " << _id << " (this is fine, don't worry about it)" << std::endl;
	} else {
		int defaultDialogue;
		
		input >> idleFrames >> frameDuration >> defaultDialogue;
		std::string line;
		std::stringstream lineStream;
		
		giveResponseIndices[-1] = defaultDialogue;
		
		while(getline(input, line)) {
			lineStream.str(line);
			lineStream.clear();
			
			char type;
			lineStream >> type;
			switch(type) {
				case 'g': // give item
					int itemId, lineNumber;
					lineStream >> itemId >> lineNumber;
					giveResponseIndices[itemId] = lineNumber;
					break;
			}
		}
		
		giveResponses = cutscenes::loadNpcItemDialogue(_id);
	}

#define PC_HEIGHT 56
#define DISPLACEMENT TILESIZE

	for(int i=0;i<4;++i){
		idle[i] = characters::createLinearAnimation(0,56*i,idleFrames,0,0,48,56, NULL, frameDuration);
	}
	
	// (int textureOffsetX, int textureOffsetY, int frames, int deltaX, int deltaY, int width, int height, animation* next, int durationMultiplier, int staticOffsetX, int staticOffsetY)
	
	walk[0] = characters::createLinearAnimation(192,0,           4,-DISPLACEMENT, 0, TILESIZE, PC_HEIGHT, idle[0], 4, TILESIZE, 0);
	walk[1] = characters::createLinearAnimation(192,PC_HEIGHT,   4, 0,-DISPLACEMENT, TILESIZE, PC_HEIGHT, idle[1], 4, 0, TILESIZE);
	walk[2] = characters::createLinearAnimation(192,PC_HEIGHT*2, 4, DISPLACEMENT, 0, TILESIZE, PC_HEIGHT, idle[2], 4, -TILESIZE, 0);
	walk[3] = characters::createLinearAnimation(192,PC_HEIGHT*3, 4, 0, DISPLACEMENT, TILESIZE, PC_HEIGHT, idle[3], 4, 0,-TILESIZE);
	
	currentAnimation = idle[0];
}

void NPC::startDialogue() const {
	cutscenes::startDialogue(dialogueStart);
}

void NPC::takeSteps(direction dir, int steps) {
	if(steps == 0) return;
	
	setAnimation(walk[dir], true);
	
	int deltax = 0;
	int deltay = 0;
	switch(dir) {
		case _up:    deltay -= 1; break;
		case _down:  deltay += 1; break;
		case _left:  deltax -= 1; break;
		case _right: deltax += 1; break;
	}
	setXY(x + deltax, y + deltay);
	
	timekeeping::schedule([this, deltax, deltay, dir, steps](){
		takeSteps(dir, steps-1);
	}, walk[dir]->frames*walk[dir]->frameDuration - 1);
}

int NPC::getGiveIndex(int item) {
	auto itr = giveResponseIndices.find(item);
	if(itr == giveResponseIndices.end()) {
		itr = giveResponseIndices.find(-1);
		if(itr == giveResponseIndices.end()) return -1;
	}
	return itr->second;
}
