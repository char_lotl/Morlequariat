#ifndef __DISPLAY__
#define __DISPLAY__

#include <iostream>
#include <vector>
#include <random>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "core.h"
#include "characters.h"
#include "display.h"
#include "buttons.h"
#include "world.h"
#include "animations.h"
#include "timekeeping.h"
#include "music.h"
#include "enemy.h"
//#include "status.h"

using namespace std;

#define VIEW_INVENTORY 1
#define VIEW_SPELL_LIST 2
#define VIEW_GROUND_ITEMS 3
#define VIEW_CHAR_STATUS 4
#define VIEW_DEATH_SCREEN 5
#define VIEW_SPELL_DESCRIPTION 6
#define VIEW_SHOP_ITEMS 7
#define VIEW_LOOK 8
#define VIEW_START_MENU 256

#define CHARSPEED 1
#define CHARWIDTH 12
#define CHARHEIGHT 20
//#define TEXTWIDTH 50

#define DMGCHARW 9
#define DMGCHARH 14

#define FONTCOUNT 4

#include <sstream>

const int TILESIZE = 48;

namespace cutscenes {
	void setAdvanceable();
	int getResponseCount();
	
	int getFullscreenR();
	int getFullscreenG();
	int getFullscreenB();
}

namespace buttons {
	void setThemeCount(int);
}

namespace display {
	
	int globalScale = 1;
	bool fullscreen = false;
	
	void toggleFullscreen() {
		fullscreen = !fullscreen;
	}
	
	bool isFullscreen() {
		return fullscreen;
	}

	namespace {
		sf::Sprite tileStamp;
		sf::RenderTexture bg_texture;
		sf::RenderTexture overlayTexture;
		sf::Sprite background;
		sf::Sprite overlay;
		
		sf::Sprite titleSplash;
		sf::Texture titleTexture;
		
		sf::RenderTexture windowTexture;
		sf::Sprite windowSprite;

		sf::Texture* tilesetTexture;
		int tilesetWidth;
		int tilesetHeight;

		sf::Texture oobTexture;
		sf::Sprite outOfBounds;
		
		sf::Texture miscUiTexture;
		sf::Sprite miscUiStamp;

		// these are in pixels now
		int offsetX = 0;
		int offsetY = 0;
		
		int _view;
		
		int mouseX, mouseY;
		
		sf::Texture meters;
		sf::Sprite health1;
		sf::Sprite health2;
		sf::Sprite spirit1;
		sf::Sprite spirit2;		
		sf::Sprite speed1;
		sf::Sprite speed2;
		sf::Sprite iconHealth;
		sf::Sprite iconSpirit;
		sf::Sprite iconSpeed;
		sf::Sprite iconAction;
		
		vector<shared_ptr<animations::AnimationInstance>> foregroundAnimations;
		vector<shared_ptr<animations::AnimationInstance>> backgroundAnimations;
		vector<shared_ptr<animations::AnimationInstance>> uiAnimations;
		vector<shared_ptr<animations::Projectile>> projectiles;
		
		sf::Texture itemPileTexture;
		sf::Sprite itemPileSprite;
		
		sf::Sprite charSprite;
		/*sf::Texture fontTexture;
		sf::Texture fontTextureRed;
		sf::Texture fontTextureBlue;*/
		sf::Texture fontTextures[FONTCOUNT];
		//list<string> dialogueTextList;
				
		sf::Texture dmgFontTexture;
		
		sf::Sprite buffIconSprite;
		sf::Texture buffIconTexture;
		
		bool isDialogue = false;
		int dialogueStartTime;
		int dialogueTickCount;
		string dialogueText;
		string dialogueName;
		int dialogueSoundId;
		int dialogueSpriteId;
		shared_ptr<vector<string> > dialogueResponseOptions;

		bool isSceneImage;
		sf::Texture sceneImageTexture;
		sf::Sprite sceneImageSprite;
		
		sf::Texture textboxTexture;
		sf::Sprite dialogueTextboxSprite;
		
		sf::Texture statusPageTexture;
		sf::Sprite statusTextboxSprite;
		
		sf::Texture talkspriteTexture;
		sf::Sprite dialogueFaceSprite;
		
		sf::Texture itemDescriptionBoxTexture;
		sf::Sprite itemDescriptionBox;
		
		sf::Texture selectTexture;
		sf::Sprite selectItemSprite;
		sf::Sprite selectCharacterSprite;
		
		sf::Texture cursorTexture;
		sf::Sprite cursorSprite;
		
		sf::Texture uiThemeListTexture;
		sf::Sprite uiThemeSprite;
		
		sf::Sprite turnIndicator;
		
		sf::Texture transientTerrainTexture;
		sf::Sprite transientTerrainSprite;

		sf::Texture dungeonBannerTexture;
		sf::Sprite dungeonBannerSprite;

		sf::Sprite lookReticle;

		Enemy* viewedEnemy;
	}
	
	//sf::Font font;
	void init(){
		//font.loadFromFile("misc/axolotl.ttf");
		tilesetTexture = new sf::Texture;
		
		windowTexture.create(SCREENWIDTH+128,SCREENHEIGHT+64);
		
		meters.loadFromFile("img/meters.png");
		health1.setTexture(meters);
		health1.setTextureRect(sf::IntRect(0,0,99,7));
		health2.setTexture(meters);
		health2.setTextureRect(sf::IntRect(100,0,103,7));
		spirit1.setTexture(meters);
		spirit1.setTextureRect(sf::IntRect(0,7,99,7));
		spirit2.setTexture(meters);
		spirit2.setTextureRect(sf::IntRect(100,7,103,7));
		speed1.setTexture(meters);
		speed1.setTextureRect(sf::IntRect(0,14,7,7));
		speed2.setTexture(meters);
		speed2.setTextureRect(sf::IntRect(7,14,7,7));
		
		iconHealth.setTexture(meters);
		iconHealth.setTextureRect(sf::IntRect(149,14,13,13));
		iconSpirit.setTexture(meters);
		iconSpirit.setTextureRect(sf::IntRect(162,14,15,15));
		iconSpeed.setTexture(meters);
		iconSpeed.setTextureRect(sf::IntRect(177,14,13,11));
		iconAction.setTexture(meters);
		iconAction.setTextureRect(sf::IntRect(190,14,11,17));
		
		itemPileTexture.loadFromFile("img/item_overworld.png");
		itemPileSprite.setTexture(itemPileTexture);
		
		for(int i=0; i<FONTCOUNT; ++i) fontTextures[i].loadFromFile("img/font/"+to_string(i)+".png");
		
		charSprite.setTexture(fontTextures[0]);
		
		dmgFontTexture.loadFromFile("img/font/dmg.png");
		
		buffIconTexture.loadFromFile("img/buffs.png");
		buffIconSprite.setTexture(buffIconTexture);
		
		textboxTexture.loadFromFile("img/textbox.png");
		dialogueTextboxSprite.setTexture(textboxTexture);
		dialogueTextboxSprite.setPosition(48,SCREENHEIGHT-191);
				
		statusPageTexture.loadFromFile("img/statuspage.png");
		statusTextboxSprite.setTexture(statusPageTexture);
		statusTextboxSprite.setPosition(48,48);
		
		talkspriteTexture.loadFromFile("img/talksprites.png");
		dialogueFaceSprite.setTexture(talkspriteTexture);
		dialogueFaceSprite.setPosition(55,SCREENHEIGHT - 184);
		
		sceneImageTexture.loadFromFile("img/scene/0.png");
		sceneImageSprite.setTexture(sceneImageTexture);
		sceneImageSprite.setPosition(217, SCREENHEIGHT - 358);
		//sceneImageSprite.setTextureRect(sf::IntRect(217, 242, 320, 128));
		
		itemDescriptionBoxTexture.loadFromFile("img/invbox.png");
		itemDescriptionBox.setTexture(itemDescriptionBoxTexture);
		itemDescriptionBox.setPosition(ITEM_IMGSIZE * 4, ITEM_IMGSIZE/2);
		
		selectTexture.loadFromFile("img/select.png");
		selectItemSprite.setTexture(selectTexture);
		selectItemSprite.setTextureRect(sf::IntRect(TILESIZE+ITEM_IMGSIZE, ITEM_IMGSIZE*3, ITEM_IMGSIZE, ITEM_IMGSIZE));
		selectCharacterSprite.setTexture(selectTexture);
		selectCharacterSprite.setTextureRect(sf::IntRect(TILESIZE+ITEM_IMGSIZE*2, 0, ITEM_IMGSIZE*2, ITEM_IMGSIZE*2));
		
		miscUiTexture.loadFromFile("img/uimisc.png");
		miscUiStamp.setTexture(miscUiTexture);
		
		titleTexture.loadFromFile("img/title.png");
		titleSplash.setTexture(titleTexture);
		
		cursorTexture.loadFromFile("img/uis/" + to_string(buttons::getUiTheme()) + "/cursor.png");
		cursorSprite.setTexture(cursorTexture);
		
		uiThemeListTexture.loadFromFile("img/uithemes.png");
		uiThemeSprite.setTexture(uiThemeListTexture);
		uiThemeSprite.setPosition(4*ITEM_IMGSIZE + 221,  ITEM_IMGSIZE/2 - 3 + 5*38);
		uiThemeSprite.setTextureRect(sf::IntRect(0,0,96,32));
		buttons::setThemeCount(uiThemeListTexture.getSize().y / 32);
		
		turnIndicator.setTexture(miscUiTexture);
		turnIndicator.setTextureRect(sf::IntRect(0,48,48,48));
		turnIndicator.setPosition(SCREENWIDTH - 48 - 3, 3);
		
		transientTerrainTexture.loadFromFile("img/terrainstatus.png");
		transientTerrainSprite.setTexture(transientTerrainTexture);
		
		dungeonBannerTexture.loadFromFile("img/dungeonbanners.png");
		dungeonBannerSprite.setTexture(dungeonBannerTexture);
		
		lookReticle.setTexture(selectTexture);
	}
	
	void setGlobalScale(int w, int h) {
	
		int xScale = w / TOTALWIDTH;
		int yScale = h / TOTALHEIGHT;
		int scale = xScale > yScale ? yScale : xScale;
	
		globalScale = scale;
		windowSprite.setScale(scale,scale);
		windowSprite.setPosition( (w - TOTALWIDTH * scale)/2, (h - TOTALHEIGHT * scale)/2);
	}
	
	int getGlobalScale() {
		return globalScale;
	}
	
	bool isClearView(){return _view==0;}
	bool isStartMenu(){return _view>=VIEW_START_MENU;}
	bool isDeathScreen(){return _view==VIEW_DEATH_SCREEN;}
	bool isLookInterface(){return _view==VIEW_LOOK;}
	void clearView(){_view = 0;}
	void showInventory(){_view = VIEW_INVENTORY;}
	void showSpellList(){_view = VIEW_SPELL_LIST;}
	void showSpellDescription(){_view = VIEW_SPELL_DESCRIPTION;}
	void showGroundItems(){_view = VIEW_GROUND_ITEMS;}
	void showShopItems(){_view = VIEW_SHOP_ITEMS;}
	void showStartMenu(){_view = VIEW_START_MENU;}
	void showLookInterface(){_view = VIEW_LOOK;}
	void showCharacterStatus(){
		viewedEnemy = NULL;
		_view = VIEW_CHAR_STATUS;
	}
	void showDeathScreen(){
		_view = VIEW_DEATH_SCREEN;
		dialogueStartTime = timekeeping::currentTick();
	}

	bool centerOnCharacter = false;
	Character* centerCharacter = NULL;

	void centerCoords(int x, int y) {
		offsetX = x*TILESIZE - SCREENWIDTH/2;
		offsetY = y*TILESIZE - SCREENHEIGHT/2;
	}
	
	void center(int x, int y){
		centerOnCharacter = false;
		centerCoords(x, y);
	}

	void center(Character* character){
		centerOnCharacter = true;
		centerCharacter = character;
	}
	
	list<pair<pair<int, int>, int>> tileUpdateQueue;
	
	void scheduleSetTile(int x, int y, int t){
		tileUpdateQueue.push_back(make_pair(make_pair(x,y),t));
	}
	
	void stampTile(int x, int y, int t) {
		tileStamp.setTextureRect(sf::IntRect((t%tilesetWidth)*TILESIZE, (t/tilesetWidth)*TILESIZE, TILESIZE, TILESIZE ));
		tileStamp.setPosition(x*TILESIZE, y*TILESIZE);
		bg_texture.draw(tileStamp);
	}
	
	void stampOverlayTile(int x, int y, int t) {
		tileStamp.setTextureRect(sf::IntRect((t%tilesetWidth)*TILESIZE, (t/tilesetWidth)*TILESIZE, TILESIZE, TILESIZE ));
		tileStamp.setPosition(x*TILESIZE, y*TILESIZE);
		overlayTexture.draw(tileStamp);
	}
	
	void setPartialTile(int x, int y, int xQuarters, int yQuarters, int t, int tXQ, int tYQ){
		tileStamp.setTextureRect(sf::IntRect((t%tilesetWidth)*TILESIZE + (tXQ * TILESIZE / 4), (t/tilesetWidth)*TILESIZE + (tYQ * TILESIZE / 4), TILESIZE/4, TILESIZE/4 ));
		tileStamp.setPosition(x*TILESIZE + (xQuarters * TILESIZE / 4), y*TILESIZE + (yQuarters * TILESIZE / 4));
		bg_texture.draw(tileStamp);
	}
	
	bool doesNotInterpolate(int baseTile, unsigned char questioned) {
		return (questioned & 0xE6) != ((baseTile & 0x06) | 0xE0);
	}
	
	void setInterpolatedTile(int x, int y, int t, unsigned char** imgIds, int width, int height) {
		if (y > 0 && doesNotInterpolate(t, imgIds[y-1][x])) {
			setPartialTile(x, y, 1, 0, t, 1, 0);
			setPartialTile(x, y, 2, 0, t, 2, 0);
		
			if (x > 0 && doesNotInterpolate(t, imgIds[y][x-1])) {
				setPartialTile(x, y, 0, 0, t, 0, 0);
				setPartialTile(x, y, 0, 1, t, 0, 1);
				setPartialTile(x, y, 0, 2, t, 0, 2);
			} else {
				setPartialTile(x, y, 0, 0, t, 1, 0);
			}
			
			if (x < width-1 && doesNotInterpolate(t, imgIds[y][x+1])) {
				setPartialTile(x, y, 3, 0, t, 3, 0);
				setPartialTile(x, y, 3, 1, t, 3, 1);
				setPartialTile(x, y, 3, 2, t, 3, 2);
			} else {
				setPartialTile(x, y, 3, 0, t, 2, 0);
			}
			
		} else {
			if (x > 0 && doesNotInterpolate(t, imgIds[y][x-1])) {
				setPartialTile(x, y, 0, 0, t, 0, 1);
				setPartialTile(x, y, 0, 1, t, 0, 1);
				setPartialTile(x, y, 0, 2, t, 0, 2);
			} else if(x > 0 && y > 0 && doesNotInterpolate(t, imgIds[y-1][x-1])) {
				setPartialTile(x, y, 0, 0, t, 1, 1);
			}
			if (x < width-1 && doesNotInterpolate(t, imgIds[y][x+1])) {
				setPartialTile(x, y, 3, 0, t, 3, 1);
				setPartialTile(x, y, 3, 1, t, 3, 1);
				setPartialTile(x, y, 3, 2, t, 3, 2);
			} else if(x < width-1 && y > 0 && doesNotInterpolate(t, imgIds[y-1][x+1])) {
				setPartialTile(x, y, 3, 0, t, 2, 1);
			}
		}
		
		if (y < height-1 && doesNotInterpolate(t, imgIds[y+1][x])) {
			setPartialTile(x, y, 1, 3, t, 1, 3);
			setPartialTile(x, y, 2, 3, t, 2, 3);
			
			if (x > 0 && doesNotInterpolate(t, imgIds[y][x-1])) {
				setPartialTile(x, y, 0, 3, t, 0, 3);
			} else {
				setPartialTile(x, y, 0, 3, t, 1, 3);
			}
			if (x < width-1 && doesNotInterpolate(t, imgIds[y][x+1])) {
				setPartialTile(x, y, 3, 3, t, 3, 3);
			} else {
				setPartialTile(x, y, 3, 3, t, 2, 3);
			}
		} else {
			if (x > 0 && doesNotInterpolate(t, imgIds[y][x-1])) {
				setPartialTile(x, y, 0, 3, t, 0, 2);
			} else if(x > 0 && y < height-1 && doesNotInterpolate(t, imgIds[y+1][x-1])) {
				setPartialTile(x, y, 0, 3, t, 1, 2);
			}
			if (x < width-1 && doesNotInterpolate(t, imgIds[y][x+1])) {
				setPartialTile(x, y, 3, 3, t, 3, 2);
			} else if(x < width-1 && y < height-1 && doesNotInterpolate(t, imgIds[y+1][x+1])) {
				setPartialTile(x, y, 3, 3, t, 2, 2);
			}
		}
	}
	
	void setTile(int x, int y, int t, unsigned char** imgIds, int width, int height){
		if(t < 0xF8 && (t & 0xF1) == 0xF1 ) {
			stampTile(x, y, t - 1);
			setInterpolatedTile(x, y, t, imgIds, width, height);
		} else if( (t & 0xF0) == 0xD0) {
			stampTile(x, y, t - 0x10);
			stampOverlayTile(x, y, t);
		} else {
			stampTile(x, y, t);
		}
	}
	

	void loadBackground(unsigned char** imgIds, unsigned char** tileIds, int width, int height, int tileset){
		bg_texture.create(width*TILESIZE, height*TILESIZE);
		bg_texture.clear(sf::Color(0,0,0,0));
		overlayTexture.create(width*TILESIZE, height*TILESIZE);
		overlayTexture.clear(sf::Color(0,0,0,0));

		tilesetTexture->loadFromFile("img/tiles/"+to_string(tileset)+".png");
		tilesetWidth = tilesetTexture->getSize().x / TILESIZE;
		tilesetHeight = tilesetTexture->getSize().y / TILESIZE;

		tileStamp.setTexture(*tilesetTexture);
		
		for(int i=0; i<height; ++i){
			for(int j=0; j<width; ++j){
				
				setTile(j, i, imgIds[i][j], imgIds, width, height);
				
				if((tileIds[i][j] & FLAG_CRATE) == FLAG_CRATE) {
					stampTile(j, i, CRATE_TILE);
				} else if((tileIds[i][j] & FLAG_CRATE) == FLAG_BREAK) {
					stampTile(j, i, FLOOR_BREAK_TILE);
				} else if((tileIds[i][j] & (FLAG_ONEWAY | FLAG_BLOCKED)) == FLAG_ONEWAY) {
					direction pushDir = (direction) ((tileIds[i][j] >> 6)%4);
					addAnimationAtTile(j, i, animations::flowingWater[pushDir], false);
				}
			}
		}

		bg_texture.display();
		overlayTexture.display();
		background = sf::Sprite(bg_texture.getTexture());
		overlay = sf::Sprite(overlayTexture.getTexture());
		
		oobTexture.loadFromFile("img/bg/"+to_string(tileset)+".png");
		oobTexture.setRepeated(true);
		outOfBounds.setTexture(oobTexture);
	}

	void updateMouse(int x, int y){
		mouseX = x / globalScale;
		mouseY = y / globalScale;
	}
	
	void addAnimation(int x, int y, const shared_ptr<animations::animation>& a){
		uiAnimations.push_back(shared_ptr<animations::AnimationInstance> { new animations::AnimationInstance(x, y, false, a) });
	}
	
	void addAnimationAtTile(int x, int y, const shared_ptr<animations::animation>& a, bool foreground){
		if(foreground) {
			foregroundAnimations.push_back(shared_ptr<animations::AnimationInstance> { new animations::AnimationInstance(x, y, true, a) });
		} else {
			backgroundAnimations.push_back(shared_ptr<animations::AnimationInstance> { new animations::AnimationInstance(x, y, true, a) });
		}
	}
	
	void removeAnimationAtTile(int x, int y, bool foreground) {
		if(foreground) {
			for(auto itr=foregroundAnimations.begin(); itr!=foregroundAnimations.end();){
				if((*itr)->offsetX == x && (*itr)->offsetY == y) {
					itr = foregroundAnimations.erase(itr); 
				} else {
					++itr;
				}
			}
		} else {
			for(auto itr=backgroundAnimations.begin(); itr!=backgroundAnimations.end();){
				if((*itr)->offsetX == x && (*itr)->offsetY == y) {
					itr = backgroundAnimations.erase(itr); 
				} else {
					++itr;
				}
			}
		}
	}
	
	void clearAnimations() {
		foregroundAnimations.clear();
		backgroundAnimations.clear();
		uiAnimations.clear();
	}
	
	int getEscapedWidth(string text){
		int width = 0;
		int line = 0;
		for(int i=0; i<text.length(); ++i) {
			if(text[i]=='\\') line-=2;
			else if(text[i] == '\n') {
				if(line > width) width = line;
				line = 0;
			}
			else ++line;
		}
		if(line > width) width = line;
		return width;
	}
	
	int getEscapedLength(string text){
		int line = 0;
		for(int i=0; i<text.length(); ++i) {
			if(text[i]=='\\') line-=2;
			else ++line;
		}
		return line;
	}
	
	string padInt(int i, int width){
		string output = to_string(i);
		while(output.length() < width) output = " " + output;
		return output;
	}

	void skipDialogue() {
		dialogueStartTime = -65536;
	}
	
	void showDialogue(const string& text, const string& name, int talkspriteid, int voiceid) {
		isDialogue = true;
		dialogueText = text;
		dialogueName = name;
		dialogueSpriteId = talkspriteid;
		dialogueSoundId = voiceid;
		dialogueStartTime = timekeeping::currentTick();
		dialogueTickCount = 0;
	}
	
	void showDialogueOptions(const shared_ptr<vector<string> >& options) {
		dialogueResponseOptions = options;
	}
	
	void hideDialogue(){
		isDialogue = false;
		isSceneImage = false;
	}
	
	void showSceneImage(int index) {
		if(index < 0) isSceneImage = false;
		else {
			isSceneImage = true;
			sceneImageTexture.loadFromFile("img/scene/"+to_string(index)+".png");
			sceneImageSprite.setTexture(sceneImageTexture);
		}
	}
	
	void setUiTheme(int theme) {
		uiThemeSprite.setTextureRect(sf::IntRect(0, 32*theme, 96, 32));
		cursorTexture.loadFromFile("img/uis/" + to_string(theme) + "/cursor.png");
		
		for(int i=0; i<FONTCOUNT; ++i) {
			if (! fontTextures[i].loadFromFile("img/uis/" + to_string(theme) + "/font/" + to_string(i) + ".png") ) {
				fontTextures[i].loadFromFile("img/font/"+to_string(i)+".png");
			}
		}
	}
	
	/*void startDialogue(const string& text){
		dialogueTextList.clear();
		dialogueTextList.push_back(text);
		dialogueStart = timekeeping::currentTick();
	}
	
	void startDialogue(list<string>& text){
		dialogueTextList = text;
		dialogueStart = timekeeping::currentTick();
	}
	
	void advanceDialogue(){
		if(dialogueTextList.size() && getEscapedWidth(*dialogueTextList.begin()) <= (timekeeping::currentTick() - dialogueStart)/CHARSPEED) {
			dialogueTextList.pop_front();
			dialogueStart = timekeeping::currentTick();
		}
	}*/

	void showEnemyStatus(Enemy* e){
		viewedEnemy = e;
		_view = VIEW_CHAR_STATUS;
	}

	struct damageText {
		int amt;
		int x;
		int y;
		int endTick;
	};
	
	vector<damageText> damageTexts;
	
	void showDamageText(int amt, int x, int y){
		damageText dt;
		dt.amt = amt;
		dt.x = x;
		dt.y = y;
		dt.endTick = timekeeping::currentTick() + 15;
		damageTexts.push_back(dt);
	}

	void sparkleHealthMeter(PlayerCharacter* pc){
		addAnimation(SCREENWIDTH+21, 128*pc->getId()+76, animations::gainHealth);
	}
	
	void sparkleSpiritMeter(PlayerCharacter* pc){
		addAnimation(SCREENWIDTH+21, 128*pc->getId()+92, animations::gainSpirit);
	}
	
	void addProjectile(shared_ptr<animations::Projectile> proj) {
		projectiles.push_back(proj);
	}
	
	void spliceStatText(string& text, int startIndex) {
		int removeLength;
		string newText;
		
		PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
		
		switch(text[startIndex]) {
			case 'r': { // range
				newText = "\\R" + to_string(pc->getRange() + stoi(text.substr(startIndex+1, 2))) + "\\c";
				removeLength = 3;
				break;
			}
			case 'a': { // outgoing attack, typed
				damage_type dt = (damage_type) (text[startIndex+1] - '0');
				newText = "\\R" + to_string(stoi(text.substr(startIndex+2, 3)) * pc->getDamageDealt(dt) / 100) + "\\S" + text[startIndex+1] + "\\c";
				removeLength = 5;
				break;
			}
			case 'd': { // incoming attack, typed
				damage_type dt = (damage_type) (text[startIndex+1] - '0');
				newText = "\\R" + to_string(stoi(text.substr(startIndex+2, 3)) * pc->getDamageTaken(dt) / 100) + "\\S" + text[startIndex+1] + "\\c";
				removeLength = 5;
				break;
			}
			case 'w': { // weapon-based attack
				weapon* w = pc->getEquippedWeapon();
				newText = "\\R" + to_string(w->effects[0].baseDamage * pc->getDamageDealt(w->effects[0].dtype) / 100) + "\\S" + (char)(w->effects[0].dtype + '0') + "\\c";
				removeLength = 1;
				break;
			}
			case 's': { // spirit
				newText = "\\R" + to_string(pc->getSpiritConsumption() * stoi(text.substr(startIndex+1, 3)) / 100) + "\\S]\\c";
				removeLength = 3;
				break;
			}
				
		}
		
		text.erase(startIndex, removeLength);
		text.insert(startIndex, newText);
	}

	void drawText(int originX, int originY, string text, int maxWidth = 50){
		
		int tileset = 0;
		
		int readPos=0;
		int writePos=0;
		while(readPos < text.length()) {
			if(text[readPos] == '\\'){
				++readPos;
				switch(text[readPos]){
					case 'R': tileset=1; break; // red
					case 'B': tileset=2; break; // blue
					case 'S': tileset=3; break; // special characters
					case 'c': tileset=0; break; // clear color
					case 'n': originY += CHARHEIGHT; writePos = 0; break; // newline
					case '\\': { // character stat escape sequences
						spliceStatText(text, readPos+1);
					}
				}
			} else if(text[readPos] == '\n'){
				originY += CHARHEIGHT;
				writePos = 0;
			} else {

				charSprite.setTexture(fontTextures[tileset]);


				charSprite.setTextureRect(sf::IntRect((text[readPos]-' ') % 12 * CHARWIDTH, (text[readPos]-' ') / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + (writePos % maxWidth) * CHARWIDTH;
				int y = originY + writePos / maxWidth * CHARHEIGHT;				
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writePos;
			}
			++readPos;
		}
	}
	
	unsigned int chaos(int x, int y) {
		const int slow = 2;
		int c = (timekeeping::currentTick()/slow << 9) + (x << 3) + y;
		
		c ^= c << 13;
		c ^= c >> 17;
		c ^= c << 5;
		
		return c >> 7;
	}
	
	void narrateText(int originX, int originY, string text, int voiceId, int maxWidth = 50){
		
		//(timekeeping::currentTick() - dialogueStart)/CHARSPEED
		
		int maxLength = (timekeeping::currentTick() - dialogueStartTime)/CHARSPEED;
		
		int tileset = 0;
		bool wave = false;
		int vibrate = 0;
		
		bool random = false;
		
		int delay = 1;
		bool voicePunctuation = false;
		
		int readPos=0;
		int writeTime=0;
		
		int writeX = 0;
		int writeY = 0;
		
		while(writeTime < maxLength && readPos < text.length()) {
			if(text[readPos] == '\\'){
				++readPos;
				switch(text[readPos]){
					case 'R': tileset=1; break; // red
					case 'B': tileset=2; break; // blue
					case 'S': tileset=3; break; // special characters
					case 'c': tileset=0; break; // clear color
					case 'W': wave = true; break;
					case 'V': vibrate += 1; break; // vibrate
					case 't': wave=false; vibrate=0; break; // clear texture
					case 'P': delay *= 2; break; // slow
					case 'i': delay = 0; break; // instantaneous
					case 'f': delay = 1; break; // normal speed
					case 'n': ++writeY; writeX=0; break; // newline
					case 'L': voicePunctuation = true; break; // Loud
					case 'q': voicePunctuation = false; break;
					case '{': random = true; break;
					case '}': random = false; break;
				}
			} else if(text[readPos] == '\n'){
				++writeY;
				writeX = 0;
			} else {
				if(writeX >= maxWidth) {
					++writeY;
					writeX = 0;
				}

				charSprite.setTexture(fontTextures[tileset]);

				unsigned char toWrite = text[readPos];
				
				if (random) {
					unsigned int c = chaos(writeX, writeY);
					switch(toWrite) {
						case 'a':
						case 'A':
							toWrite += c % 26;
							break;
						case '0':
							toWrite += c % 10;
							break;
						case '!':
							toWrite = "!#%&*<>^_`?@~"[c%13];
							break;
					}
				}
				
				toWrite -= ' ';

				charSprite.setTextureRect(sf::IntRect(toWrite % 12 * CHARWIDTH, toWrite / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + writeX * CHARWIDTH;
				int y = originY + writeY * CHARHEIGHT;
				if(vibrate) {
					x += rand() % (2*vibrate+1) - (vibrate+1);
					y += rand() % (2*vibrate+1) - (vibrate+1);
				} else if (wave) {
					int wavePos = (timekeeping::currentTick()+3*writeX) % 21 - 5;
					if(wavePos > 5) wavePos = 10 - wavePos;
					y += wavePos;
				}
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writeX;
				
				writeTime += delay;
			}
			++readPos;
		}
		
		if(writeTime == maxLength && maxLength > dialogueTickCount) {
			if((voicePunctuation && text[readPos-1] != ' ') || (text[readPos-1] >= 'A' && text[readPos-1] <= 'z')) {
				double pitch = 1;
				
				if(wave) {
					pitch += .01 * (writeTime % 21);
				} else if(vibrate) {
					pitch += (rand() % 64) / 128.0 - (32/128.0);
				} else if(voiceId >= -1) {
					pitch += (rand() % 16) / 128.0 - (8/128.0);
				}
				
				if(tileset == 1 || tileset == 2) {
					pitch += .1;
				}
				
				
				if(voiceId < -1) voiceId = -1;
				
				music::playSound("voice/"+to_string(voiceId), pitch);
			}
			dialogueTickCount = maxLength;
			
		} else if(readPos == text.length() && _view != VIEW_DEATH_SCREEN) {
			cutscenes::setAdvanceable();
		}
	}
	
	void drawItemInfoPanel(ItemOrWeapon* i){
		windowTexture.draw(itemDescriptionBox);
		if(!i) return;
		drawText(4*ITEM_IMGSIZE + 7, ITEM_IMGSIZE/2 + 6, "\\B"+i->getName());
		drawText(4*ITEM_IMGSIZE + 7, ITEM_IMGSIZE/2 + 33, i->getFlavor(), (ITEM_IMGSIZE*6 - 14)/CHARWIDTH + 1);
	}
	
	void drawSpellInfoPanel(shared_ptr<spell> s){
		//i->getSprite()->setPosition(8*ITEM_IMGSIZE, SCREENHEIGHT);
		//windowTexture.draw(*(i->getSprite()));
		//drawText(8*ITEM_IMGSIZE, SCREENHEIGHT, "\\B"+i->getName());
		drawText(200, SCREENHEIGHT - 128, s->description, (SCREENWIDTH - 200)/CHARWIDTH);
	}
	
	bool hasDialogue() {
		return isDialogue;
	}
	
	void drawButtons() {
		const unordered_set<Button*>& visibleButtons = buttons::listButtons();
		for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr) windowTexture.draw(*(*itr)->getSprite());
	}
	
	void drawPauseMenuUnder() {
		windowTexture.draw(itemDescriptionBox);
		drawText(4*ITEM_IMGSIZE + 7, ITEM_IMGSIZE/2 + 6, isStartMenu() ? "\\BSettings" : "\\BGame paused");
	}
	
	void drawPauseMenuOver() {
		const int rowPad = 38;
		
		if (!isStartMenu()) {
			drawText(4*ITEM_IMGSIZE + 30,  ITEM_IMGSIZE/2 + 1 + rowPad, "Return to Game");
			drawText(4*ITEM_IMGSIZE + 7, ITEM_IMGSIZE/2 + 39 + rowPad*buttons::getSelectedMenuItem(), "\\S+");
		}
		
		drawText(4*ITEM_IMGSIZE + 19,  ITEM_IMGSIZE/2 + 1 + 2*rowPad, "Music Volume");
		int mVolume = music::getMusicVolume();
		drawText(4*ITEM_IMGSIZE + 221, ITEM_IMGSIZE/2 + 1 + 2*rowPad, mVolume ? " " + to_string(mVolume) : "OFF");
		
		drawText(4*ITEM_IMGSIZE + 19,  ITEM_IMGSIZE/2 + 1 + 3*rowPad, "Sound Volume");
		int sVolume = music::getSoundVolume();
		drawText(4*ITEM_IMGSIZE + 221, ITEM_IMGSIZE/2 + 1 + 3*rowPad, sVolume ? " " + to_string(sVolume) : "OFF");
		
		drawText(4*ITEM_IMGSIZE + 30,  ITEM_IMGSIZE/2 + 1 + 4*rowPad, fullscreen ? "Press F11 to Disable Fullscreen" : "Press F11 to Enable Fullscreen");
		
		drawText(4*ITEM_IMGSIZE + 30,  ITEM_IMGSIZE/2 + 1 + 5*rowPad, "UI Theme");
		
		windowTexture.draw(uiThemeSprite);
		
		drawText(4*ITEM_IMGSIZE + 30,  ITEM_IMGSIZE/2 + 1 + 7*rowPad, isStartMenu() ? "BACK" : "QUIT TO TITLE");

		drawText(4*ITEM_IMGSIZE + 18,  ITEM_IMGSIZE/2 + 1 + 8*rowPad,
			"Move with WASD.\nPress E to inspect things and\n    advance dialogue.\nPress Q to open the inventory.\nPress tab to end turn."
		);
	}
	
	void drawTitleScreen(sf::RenderWindow& window) {
		window.clear(sf::Color(0,0,0));
		windowTexture.clear(sf::Color(0,0,0));
		windowTexture.draw(titleSplash);
		
		drawText(0, SCREENHEIGHT + ITEM_IMGSIZE - CHARHEIGHT, "Alpha 4.3.0");
		
		if (buttons::isPaused()) {
			drawPauseMenuUnder();
		}

		drawButtons();
		
		if (buttons::isPaused()) {
			drawPauseMenuOver();
		} else {
				for(int i=0; i<4; ++i) {
				string saveSlotText = "";
				if(i == buttons::getSelectedItem()) saveSlotText += "\\B";
				saveSlotText += "Save File " + to_string(i);
				if(!buttons::saveFileExists(i))saveSlotText += " (Empty)";
			
				drawText((TOTALWIDTH-256)/2 + 7, 251 + 36*i, saveSlotText);
			}
			drawText((TOTALWIDTH-256)/2 + 7, TOTALHEIGHT - ITEM_IMGSIZE*2 + 6, "SETTINGS");

			drawText((TOTALWIDTH-256)/2 + 7, TOTALHEIGHT - ITEM_IMGSIZE + 6, "EXIT GAME");
		}

		cursorSprite.setPosition(mouseX, mouseY);
		windowTexture.draw(cursorSprite);
		
		windowTexture.display();
		windowSprite.setTexture(windowTexture.getTexture());
		
		window.draw(windowSprite);
		window.display();
	}
	
	void drawAnimations(vector<shared_ptr<animations::AnimationInstance>>& animations) {
		for(auto itr=animations.begin(); itr!=animations.end();){
			if(!*itr) {
				// Not sure why this would ever be null, but it's causing segfaults... maybe a race condition?
				itr = animations.erase(itr);
				std::cout << "Found a null animation. This should never happen? But if it does, it won't break anything" << std::endl;
			} else {
				sf::Sprite* s = (*itr)->update();
				if(s) {
					if((*itr)->atTile) {
						int ox = (*itr)->offsetX * TILESIZE - offsetX;
						int oy = (*itr)->offsetY * TILESIZE - offsetY;
						if (ox >= SCREENWIDTH || oy >= SCREENHEIGHT) {
							++itr;
							continue;
						}
						s->setPosition(ox, oy);
					}
					windowTexture.draw(*s);
					++itr;
				}
				else {
					itr = animations.erase(itr);
				}
			}
		}
	}
	
	void drawWindow(sf::RenderWindow& window){
		// Handle queued tile updates
		while(!tileUpdateQueue.empty()){
			pair<pair<int,int>,int> update = tileUpdateQueue.front();
			tileUpdateQueue.pop_front();
			setTile(update.first.first, update.first.second, update.second, world::getAllTileImages(), world::width, world::height);
		}
		bg_texture.display();
		
		windowTexture.clear(sf::Color(103,103,103));
		
		outOfBounds.setTextureRect(sf::IntRect(offsetX/2,offsetY/2,SCREENWIDTH, SCREENHEIGHT));
		windowTexture.draw(outOfBounds);
		
		if(centerOnCharacter) {
			offsetX = centerCharacter->getX() * TILESIZE - SCREENWIDTH/2 + TILESIZE/2;
			offsetY = centerCharacter->getY() * TILESIZE - SCREENHEIGHT/2 + TILESIZE/2;
			if(centerCharacter->isCameraPixelOffset()) {
				offsetX -= centerCharacter->getOffsetX();
				offsetY -= centerCharacter->getOffsetY();
			}
		}
		
		string tooltipText = "";
		
		// Health and spirit
		for(int i=0; i<characters::playable.size(); ++i){
			
			health1.setPosition(SCREENWIDTH+22, 128*i+76);
			windowTexture.draw(health1);
			spirit1.setPosition(SCREENWIDTH+22, 128*i+92);
			windowTexture.draw(spirit1);
			PlayerCharacter* pc = characters::playable[i];
			health2.setPosition(SCREENWIDTH+21+(100*pc->getHealth()/pc->getMaxHealth()), 128*i+76);
			windowTexture.draw(health2);
			
			spirit2.setPosition(SCREENWIDTH+21+(100*pc->getSpirit()/pc->getMaxSpirit()), 128*i+92);
			windowTexture.draw(spirit2);			
		}
		
		background.setPosition(-offsetX, -offsetY);
		background.setTextureRect(sf::IntRect(0,0, min((int)bg_texture.getSize().x, SCREENWIDTH+offsetX), min((int)bg_texture.getSize().y, SCREENHEIGHT+offsetY)));
		windowTexture.draw(background); // need to cast to int because they were uint
		
		drawAnimations(backgroundAnimations);

		int mouseXTile = (mouseX + offsetX) / TILESIZE;
		if (mouseX + offsetX < 0) mouseXTile -= 1;
		int mouseYTile = (mouseY + offsetY) / TILESIZE;
		if (mouseY + offsetY < 0) mouseYTile -= 1;

		for(int i=0; i <= SCREENWIDTH/TILESIZE; ++i){
			for(int j=0; j <= SCREENHEIGHT/TILESIZE; ++j){
				int x = i + offsetX/TILESIZE;
				int y = j + offsetY/TILESIZE;
				int p = world::itemPileSpriteIndex(x,y);
				
				// TODO think about whether this should be treated as an animation or as something else
				int transient = world::getTransientTerrain(x, y);
				if (transient) {
					transientTerrainSprite.setTextureRect(sf::IntRect(TILESIZE * ((timekeeping::currentTick() / 4) % 2), transient * TILESIZE, TILESIZE, TILESIZE));
					transientTerrainSprite.setPosition(x*TILESIZE-offsetX, y*TILESIZE-offsetY);
					windowTexture.draw(transientTerrainSprite);
				}
				
				
				if(p>0){
					itemPileSprite.setPosition(x*TILESIZE-offsetX, y*TILESIZE-offsetY);
					for(int q=0;1<<q<=p;++q) if (p&(1<<q)) {
						
						itemPileSprite.setTextureRect(sf::IntRect(
								q%(itemPileTexture.getSize().x/TILESIZE) * TILESIZE,
								q/(itemPileTexture.getSize().x/TILESIZE) * TILESIZE,
								TILESIZE,TILESIZE));
						windowTexture.draw(itemPileSprite);
					}
					
					if(x == mouseXTile && y == mouseYTile){
						if(world::countItemsAt(x,y) > 6){
							tooltipText = to_string(world::countItemsAt(x,y)) + " items...\n";
						} else {
							for(int i=0; i<world::countItemsAt(x,y); ++i){
								tooltipText += world::getItemsAt(x,y).at(i)->getName() + '\n';
							}
						}
					}
				}
			}
		}
		
		vector<Character*> charsByPosition;
		
		for(int i=0; i<characters::playable.size(); ++i){
			if (!world::isCombat() && buttons::isTravelMode() && i != buttons::getSelectedCharacter()) continue;
		
			PlayerCharacter* c = characters::playable[i];
			if (c->getRoomId() != world::getRoomId()) continue; // they're in the next room, don't draw them
			
			// draw offscreen doors
			if (c->getX() == 0 && world::getDoorIndex(-1, c->getY()) >= 0) {
				miscUiStamp.setTextureRect(sf::IntRect(0,0,TILESIZE,TILESIZE));
				miscUiStamp.setPosition((c->getX()-1)*TILESIZE-offsetX, c->getY()*TILESIZE-offsetY);
				windowTexture.draw(miscUiStamp);
			}
			if (c->getY() == 0 && world::getDoorIndex(c->getX(), -1) >= 0) {
				miscUiStamp.setTextureRect(sf::IntRect(TILESIZE,0,TILESIZE,TILESIZE));
				miscUiStamp.setPosition(c->getX()*TILESIZE-offsetX, (c->getY()-1)*TILESIZE-offsetY);
				windowTexture.draw(miscUiStamp);
			}
			if (c->getX() == world::width - 1 && world::getDoorIndex(world::width, c->getY()) >= 0) {
				miscUiStamp.setTextureRect(sf::IntRect(TILESIZE*2,0,TILESIZE,TILESIZE));
				miscUiStamp.setPosition((c->getX()+1)*TILESIZE-offsetX, c->getY()*TILESIZE-offsetY);
				windowTexture.draw(miscUiStamp);
			}
			if (c->getY() == world::height - 1 && world::getDoorIndex(c->getX(), world::height) >= 0) {
				miscUiStamp.setTextureRect(sf::IntRect(TILESIZE*3,0,TILESIZE,TILESIZE));
				miscUiStamp.setPosition(c->getX()*TILESIZE-offsetX, (c->getY()+1)*TILESIZE-offsetY);
				windowTexture.draw(miscUiStamp);
			}
			
			c->updateAnimation();
			c->getSprite()->setPosition(c->getX()*TILESIZE-offsetX + c->getOffsetX(), c->getY()*TILESIZE-offsetY + c->getOffsetY() - (int)c->getSprite()->getLocalBounds().height + TILESIZE);
			if(c->getSprite()->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
				tooltipText += c->getName()+'\n';
			}
			auto itr = charsByPosition.begin();
			while(itr != charsByPosition.end() && (*itr)->getY() < c->getY()) ++itr;
			charsByPosition.insert(itr, c);

		}
		
		const vector<shared_ptr<NPC>>& npcs = world::getNPCs();
		for(int i=0; i<npcs.size(); ++i){
			shared_ptr<NPC> c = npcs[i];
			c->updateAnimation();
			c->getSprite()->setPosition(c->getX()*TILESIZE-offsetX + c->getOffsetX(), c->getY()*TILESIZE-offsetY -(int)c->getSprite()->getLocalBounds().height + TILESIZE + c->getOffsetY());
			windowTexture.draw(*c->getSprite());
			//if(c->getSprite()->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
			//	tooltipText += c->getName() + '\n';
			//}
			auto itr = charsByPosition.begin();
			while(itr != charsByPosition.end() && (*itr)->getY() < c->getY()) ++itr;
			charsByPosition.insert(itr, c.get());
		}
		
		for(int i=0; i<charsByPosition.size(); ++i) windowTexture.draw(*(charsByPosition[i]->getSprite()));
		
		const vector<Enemy*>& enemies = world::getEnemies();
		for(int i=0; i<enemies.size(); ++i){
			Enemy* c = enemies[i];
			if(c->getHealth() <= 0) continue; // don't draw dead enemies
			if(c->hasFlags(ENEMY_INVIS)) continue; // don't draw invisible enemies either
			c->updateAnimation();
			c->getSprite()->setPosition(c->getX()*TILESIZE-offsetX, c->getY()*TILESIZE-offsetY);
			windowTexture.draw(*c->getSprite());
			if(c->getSprite()->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
				tooltipText += c->getName() + ", HP: " + to_string(c->getHealth()) + "/" + to_string(c->getMaxHealth()) + '\n';
			}
			const vector<statusCondition>& conditions = c->getStatusEffects();
			int buffStart = c->getX() * TILESIZE - offsetX + (TILESIZE - conditions.size()*17)/2;
			for(int j=0; j<conditions.size(); ++j){
				buffIconSprite.setTextureRect(sf::IntRect(16*(conditions[j].id%16), 16*(conditions[j].id/16),16,16));
				buffIconSprite.setPosition(buffStart + 17*j, c->getY()*TILESIZE - offsetY - 16);
				windowTexture.draw(buffIconSprite);
			}
		}
		
		overlay.setPosition(-offsetX, -offsetY);
		overlay.setTextureRect(sf::IntRect(0,0, min((int)overlayTexture.getSize().x, SCREENWIDTH+offsetX), min((int)overlayTexture.getSize().y, SCREENHEIGHT+offsetY)));
		windowTexture.draw(overlay); // need to cast to int because they were uint
		
		drawAnimations(foregroundAnimations);
		
		for(auto itr=projectiles.begin(); itr!=projectiles.end();){
			if((*itr)->isDone()) {
				itr = projectiles.erase(itr);
			} else {
				sf::Sprite* s = (*itr)->getSprite();
				s->setPosition((*itr)->getCurrentX() - offsetX, (*itr)->getCurrentY() - offsetY);
				windowTexture.draw(*s);
				++itr;
			}
		}
		
		int dungeonId = world::getDungeonId() - 1;
		if (dungeonId >= 0) {
			dungeonBannerSprite.setTextureRect(sf::IntRect((dungeonId % 8) * 96, (dungeonId / 8) * 48, 96, 48));
			windowTexture.draw(dungeonBannerSprite);
		}
		
		for(auto itr=damageTexts.begin(); itr!=damageTexts.end();){
			int now = timekeeping::currentTick();
			if(now > itr->endTick) {
				itr = damageTexts.erase(itr); 
			}
			else {
				string text = to_string(itr->amt);
				
				int baseX = itr->x * TILESIZE - offsetX + (TILESIZE - getEscapedWidth(text)*DMGCHARW)/2;
				charSprite.setTexture(dmgFontTexture);
				
				for(int i = 0; i < text.length(); ++i){
					if(text[i] == '-') {
						charSprite.setTextureRect(sf::IntRect(DMGCHARW*10, DMGCHARH, DMGCHARW, DMGCHARH));
					} else if (text[i] == '0' && text.length() == 1) {
						charSprite.setTextureRect(sf::IntRect(DMGCHARW*10, 0, DMGCHARW, DMGCHARH));
					} else {
						charSprite.setTextureRect(sf::IntRect((text[i]-'0')*DMGCHARW, DMGCHARH*(itr->amt < 0), DMGCHARW, DMGCHARH));
					}
					charSprite.setPosition(baseX + i*DMGCHARW, itr->y*TILESIZE-offsetY-30 + (itr->endTick- now)*3);
					windowTexture.draw(charSprite);
				}
				++itr;
			}
		}
		
		if(_view == VIEW_DEATH_SCREEN) {
			narrateText((SCREENWIDTH - 40*12) / 2,(SCREENHEIGHT - 20) / 2, "But this is not how the story ends . . .", -1, 40);
			
		} else if(buttons::getSelectedCharacter() >= 0) {

			PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
			
			if(_view == VIEW_INVENTORY || _view == VIEW_SHOP_ITEMS) {
				
				drawText(ITEM_IMGSIZE/2, ITEM_IMGSIZE/2 - CHARHEIGHT, "\\BEquipment");
				for(int i=0; i<EQUIPSPACE; ++i){
					if(pc->getEquipment()[i] > -1){
						sf::Sprite* s = items::getItem(pc->getEquipment()[i])->getSprite();
						s->setPosition(i*ITEM_IMGSIZE + ITEM_IMGSIZE/2, ITEM_IMGSIZE/2);
						windowTexture.draw(*s);
						if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
							tooltipText = items::getItem(pc->getEquipment()[i])->getName();
						}
					}
				}
				
				drawText(ITEM_IMGSIZE/2, ITEM_IMGSIZE*2 - CHARHEIGHT, "\\BInventory");
				for(int i=0; i<pc->getInventory().size();++i){
					item* it = pc->getInventory()[i];
					
					if(it){
						sf::Sprite* s = it->getSprite();
						s->setPosition((i%3)*ITEM_IMGSIZE + ITEM_IMGSIZE/2, (i/3 + 2)*ITEM_IMGSIZE);
						windowTexture.draw(*s);
						if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
							tooltipText = it->getName();
						}
					}
				}
				
				drawText(ITEM_IMGSIZE/2, ITEM_IMGSIZE*13/2 - CHARHEIGHT, "\\BWeapons");
				sf::Sprite* s = pc->getEquippedWeapon()->getSprite();
				s->setPosition(ITEM_IMGSIZE/2 + ITEM_IMGSIZE * 2, ITEM_IMGSIZE*13/2);
				windowTexture.draw(*s);
				if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
					tooltipText = pc->getEquippedWeapon()->getName();
				}
				
				const vector<weapon*> weapons = pc->getOffhandWeapons();
				for(int i=0; i<weapons.size();++i){
					if(weapons[i]){
						s = weapons[i]->getSprite();
						s->setPosition(ITEM_IMGSIZE/2 + i*ITEM_IMGSIZE, ITEM_IMGSIZE*13/2);
						windowTexture.draw(*s);
						if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
							tooltipText = weapons[i]->getName();
						}
					}
				}

				string coinString = "";
				for (int i=0; i<COIN_TYPE_COUNT; ++i) {
					int coinCount = characters::getCoinCount((coinType)i);
					if (coinCount > 0) {
						coinString += (i==0) ? " " : ", ";
						coinString += to_string(coinCount);
						coinString += items::coinTypeNames[i];
					}
				}
				if (coinString.size() == 0) coinString = "0";
				drawText(ITEM_IMGSIZE*4, ITEM_IMGSIZE/2 - CHARHEIGHT, "\\BCoins:\\c" + coinString);

				// Draw item info panel
				if(buttons::getSelectedItem() >= 0) {
					int selectedItem = buttons::getSelectedItem();
					if(selectedItem < SELECT_EQUIPMENT){
						drawItemInfoPanel(pc->getInventory()[selectedItem]);
					} else if(selectedItem < SELECT_WEAPON) {
						if(pc->getEquipment()[selectedItem - SELECT_EQUIPMENT] > 0) drawItemInfoPanel(items::getItem(pc->getEquipment()[selectedItem - SELECT_EQUIPMENT]));
					} else if(selectedItem == SELECT_WEAPON + WEAPONSPACE) {
						drawItemInfoPanel(pc->getEquippedWeapon());
					} else if(selectedItem < SELECT_GROUND) {
						if(weapons[selectedItem - SELECT_WEAPON] != NULL) drawItemInfoPanel(weapons[selectedItem - SELECT_WEAPON]);
					}
				}
			}
			
			if (_view == VIEW_SHOP_ITEMS) {
				int shopItemCount = world::countShopItems();
				
				drawText(ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 - CHARHEIGHT, "\\BShop");
				
				for(int i=0; i<GROUNDSPACE && i<shopItemCount; ++i) {
					ItemOrWeapon* iow = world::getShopItem(i + buttons::getGroundScrollOffset());
					sf::Sprite* s = iow->getSprite();
					s->setPosition(ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 * (3 + 3*i));
					windowTexture.draw(*s);
					if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
						tooltipText = iow->getName();
					}
				}
				
				if(buttons::getSelectedItem() >= SELECT_GROUND) {
					int shopItemIndex = buttons::getSelectedItem() - SELECT_GROUND;
					drawItemInfoPanel(world::getShopItem(buttons::getSelectedItem() - SELECT_GROUND));
					if (world::getShopQuantity(shopItemIndex) > 0) {
						drawText(264,368, "\\R" + to_string(world::getShopQuantity(shopItemIndex)) + " available");
					}
					drawText(264,388, "\\RCost: " + to_string(world::getShopPrice(shopItemIndex)) + items::coinTypeNames[(int)world::getShopCurrency(shopItemIndex)]);
				}

			} else if(_view == VIEW_GROUND_ITEMS || (_view == VIEW_INVENTORY && world::countItemsAt(pc->getX(), pc->getY()) > 0)) {
				const vector<ItemOrWeapon*> pile = world::getItemsAt(pc->getX(), pc->getY());
				
				drawText(ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 - CHARHEIGHT, "\\BGround");
				
				for(int i=0; i<GROUNDSPACE && i<pile.size(); ++i) {
					sf::Sprite* s = pile[i + buttons::getGroundScrollOffset()]->getSprite();
					s->setPosition(ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 * (3 + 3*i));
					windowTexture.draw(*s);
					if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
						tooltipText = pile[i + buttons::getGroundScrollOffset()]->getName();
					}
				}
				
				if(buttons::getSelectedItem() >= SELECT_GROUND) {
					drawItemInfoPanel(pile[buttons::getSelectedItem() - SELECT_GROUND]);
				}
			} else if(_view == VIEW_CHAR_STATUS) {
				windowTexture.draw(statusTextboxSprite);
				stringstream infopile; // infopile rhymes with monopoly
				if(viewedEnemy) {
					infopile << viewedEnemy->getName() << "\n";
					infopile << "Health: " << viewedEnemy->getHealth() << "/" << viewedEnemy->getMaxHealth() << "\n";
					infopile << "Attack: " << viewedEnemy->getAttackStrength() << "\\S" << viewedEnemy->getAttackType() << "\\c\n";
					infopile << "Speed: " << viewedEnemy->getSpeed() << "\nRange: " << viewedEnemy->getRange();
					infopile << "\n\nPercent damage taken:\n   \\S0    1    2    3    4    5 \\c\n";
					for(int i=0;i<6;++i) infopile << padInt(viewedEnemy->getDamageTaken((damage_type)i), 5);
					
					infopile << "\n";

					const vector<statusCondition>& statusConditions = viewedEnemy->getStatusEffects();
					for(int i=0; i<statusConditions.size(); ++i){
						infopile << status::names[statusConditions[i].id] << ": " << statusConditions[i].intensity << "\n";
					}
				} else {
					infopile << pc->getName() << "\n\nHealth: " << pc->getHealth() << "/" << pc->getMaxHealth() << "\nSpirit: " << pc->getSpirit() << "/" << pc->getMaxSpirit() << "\n\nSpeed: " << pc->stepsLeft() << "/" << pc->getSpeed() << "\nRange: " << pc->getRange() << "\n                \\S0    1    2    3    4    5 \\c\nDamage Taken:";
					for(int i=0;i<6;++i) infopile << padInt(pc->getDamageTaken((damage_type)i), 5);
					infopile << "\nDamage Dealt:";
					for(int i=0;i<6;++i) infopile << padInt(pc->getDamageDealt((damage_type)i), 5);

					infopile << "\n";

					const vector<statusCondition>& statusConditions = pc->getStatusEffects();
					for(int i=0; i<statusConditions.size(); ++i){
						infopile << status::names[statusConditions[i].id] << ": " << statusConditions[i].intensity << "\n";
					}
				}
				
				drawText(55,55,infopile.str());
			} else if(_view == VIEW_SPELL_DESCRIPTION) {
				if(buttons::getSelectedItem() >= 0){
					drawSpellInfoPanel(spells::get(buttons::getSelectedItem()));
				}
			}
		}
		
		if(isDialogue) windowTexture.draw(dialogueTextboxSprite);
		
		if(buttons::isPaused()) {
			drawPauseMenuUnder();
		}
			
		drawButtons();
		
		// need to draw this over buttons
		if(_view == VIEW_SPELL_LIST) { // not that, actually. rather, check what button is mouseovered and display the corresponding text
			PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
			const vector<shared_ptr<spell>>& spells = pc->getSpells();
			for(int i=0; i<spells.size(); ++i) {
				string spellName = to_string(i+1) + ". ";
				if(spells[i]) {
					if(pc->canSpendSpirit(spells[i]->cost)) {
						spellName += "\\B" + spells[i]->name + "\n \\S]\\c" + to_string(spells[i]->cost * pc->getSpiritConsumption() / 100); // todo could put a heart icon if bloodstone conduit equipped
					} else {
						spellName += spells[i]->name + "\n \\S^\\c" + to_string(spells[i]->cost * pc->getSpiritConsumption() / 100);
					}
					// TODO too many magic numbers, what is going on here
					if(mouseX < 190 && mouseY >= SCREENHEIGHT + 43 * (i-3) && mouseY <= SCREENHEIGHT + 43 * (i-2)  ) {
						drawSpellInfoPanel(pc->getSpells()[i]);
					}
				}
				drawText(6, (SCREENHEIGHT-43*3 + 4) + 43*i, spellName);
			}
		}
		
		if(buttons::hasWeaponCommitButtons() && buttons::getSelectedItem() >= 0) {
			selectItemSprite.setPosition(ITEM_IMGSIZE * buttons::getSelectedItem(), SCREENHEIGHT - ITEM_IMGSIZE);
			windowTexture.draw(selectItemSprite);
		}
		
		if((_view == VIEW_INVENTORY || (_view == VIEW_GROUND_ITEMS && !buttons::hasWeaponCommitButtons())) && buttons::getSelectedItem() >= 0) {
			int selectedItem = buttons::getSelectedItem();
			int selectedItemX = 0;
			int selectedItemY = 0;
			if(selectedItem < SELECT_EQUIPMENT){
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * (selectedItem % INVENTORY_ROW);
				selectedItemY = ITEM_IMGSIZE * (selectedItem / INVENTORY_ROW + 2);
			} else if(selectedItem < SELECT_WEAPON) {
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * (selectedItem - SELECT_EQUIPMENT);
				selectedItemY = ITEM_IMGSIZE/2;
			} else if(selectedItem == SELECT_WEAPON + WEAPONSPACE) {
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * WEAPONSPACE;
				selectedItemY = ITEM_IMGSIZE * 13/2;
			} else if(selectedItem < SELECT_GROUND) {
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * (selectedItem - SELECT_WEAPON);
				selectedItemY = ITEM_IMGSIZE * 13/2;
			} else {
				selectedItemX = ITEM_IMGSIZE * 21/2;
				selectedItemY = ITEM_IMGSIZE/2 * (3 + 3 * (selectedItem - SELECT_GROUND));
			}
			selectItemSprite.setPosition(selectedItemX, selectedItemY);
			windowTexture.draw(selectItemSprite);
		}
		
		// Various icons on player cards
		for(int i=0; i<characters::playable.size(); ++i){
			PlayerCharacter* pc = characters::playable[i];
			
			if(pc->getHealth() > 0){
				iconHealth.setPosition(SCREENWIDTH+6, 128*i+73);
				windowTexture.draw(iconHealth);
			}
			
			if(pc->getSpirit() > 0){
				iconSpirit.setPosition(SCREENWIDTH+5, 128*i+88);
				if(pc->getHealth() > 0) windowTexture.draw(iconSpirit);
			}
			
			if(pc->stepsLeft() > 0 || pc->getHealth() == 0){
				iconSpeed.setPosition(SCREENWIDTH+7, 128*i+106);
				windowTexture.draw(iconSpeed);
			}
			for(int j=0; j<pc->getSpeed(); ++j){
				sf::Sprite* speedTemp = &(j<pc->stepsLeft() ? speed1 : speed2);
				speedTemp->setPosition(SCREENWIDTH+21+j*8, 128*i+108);
				windowTexture.draw(*speedTemp);
			}
			
			if(!pc->hasActed()){
				iconAction.setPosition(SCREENWIDTH+108, 128*i+104);
				windowTexture.draw(iconAction);
			}
			
			const vector<statusCondition>& conditions = pc->getStatusEffects();
			for(int j=0; j<conditions.size(); ++j){
				buffIconSprite.setTextureRect(sf::IntRect(16*(conditions[j].id%16), 16*(conditions[j].id/16),16,16));
				buffIconSprite.setPosition(SCREENWIDTH + 71 + 17*(j%3), 128*i+7 + 17*(j/3));
				windowTexture.draw(buffIconSprite);
			}
			if(i == buttons::getSelectedCharacter()) {
				selectCharacterSprite.setPosition(SCREENWIDTH, ITEM_IMGSIZE * 2 * i);
				windowTexture.draw(selectCharacterSprite);
			}
		}
		
		drawAnimations(uiAnimations);
		
		//if(buttons::getSelectedCharacter() == -1){
		//	sf::Text t("Select a character...", font, 24);
		//	t.setPosition(64*4, SCREENWIDTH+20);
		//	t.setFillColor(sf::Color::Black);
		//	windowTexture.draw(t);
		//}
		
		if(world::isCombat()) {
			turnIndicator.setTextureRect(sf::IntRect(48 * world::isPlayerTurn(), 48, 48, 48));
			windowTexture.draw(turnIndicator);
		}
		
		
		if(buttons::isPaused()) {
			drawPauseMenuOver();
		} else {
			if(isDialogue) {
				int textboxStyleOffset = 0;
				if(dialogueName.length() == 0) textboxStyleOffset = 318;
				else if(dialogueSpriteId < 0) textboxStyleOffset = 159;
				// TODO don't hardcode some of these numbers
				dialogueTextboxSprite.setTextureRect(sf::IntRect(0, textboxStyleOffset, 657, 159));

				if(dialogueSpriteId >= 0){
					int talkspritesWidth = talkspriteTexture.getSize().x / ITEM_IMGSIZE;
					dialogueFaceSprite.setTextureRect(sf::IntRect((dialogueSpriteId%talkspritesWidth)*ITEM_IMGSIZE, (dialogueSpriteId/talkspritesWidth)*ITEM_IMGSIZE, ITEM_IMGSIZE,ITEM_IMGSIZE));
					windowTexture.draw(dialogueFaceSprite);
					drawText(124,SCREENHEIGHT - 185, dialogueName);
					
					narrateText(124,SCREENHEIGHT - 158, dialogueText, dialogueSoundId, 48);
				} else if(dialogueSpriteId == -3) { // full-screen dialogue box
					windowTexture.clear(sf::Color(cutscenes::getFullscreenR(),cutscenes::getFullscreenG(),cutscenes::getFullscreenB()));
					narrateText(CHARWIDTH * 10, (SCREENHEIGHT - CHARHEIGHT)/3, dialogueText, dialogueSoundId, SCREENWIDTH/CHARWIDTH);
				} else if(dialogueName.length() > 0) {
					drawText(55,SCREENHEIGHT - 185, dialogueName);
					narrateText(55,SCREENHEIGHT - 158, dialogueText, dialogueSoundId, 54);
				} else {
					narrateText(55,SCREENHEIGHT - 185, dialogueText, dialogueSoundId, 54);
				}
			
				for(int i=0; i<cutscenes::getResponseCount(); ++i) {
					drawText(123, SCREENHEIGHT - 118 + 20*i, (*dialogueResponseOptions)[i]);
				}
				
				if(buttons::getSelectedMenuItem() >= 0) {
					drawText(105, SCREENHEIGHT - 118 + 20*buttons::getSelectedMenuItem(), "\\S+");
				}
				
				if(isSceneImage) {
					windowTexture.draw(sceneImageSprite);
				}
			}
			
			if(_view == VIEW_LOOK) {
				lookReticle.setPosition(mouseXTile*TILESIZE - offsetX, mouseYTile*TILESIZE - offsetY);
				Enemy* enemy = world::getEnemyAt(mouseXTile, mouseYTile);
				if(enemy && !(enemy->hasFlags(ENEMY_INVIS))) {
					lookReticle.setTextureRect(sf::IntRect(176, 176, TILESIZE, TILESIZE));
				} else if (world::getNPCAt(mouseXTile, mouseYTile)) {
					lookReticle.setTextureRect(sf::IntRect(176, 128, TILESIZE, TILESIZE));
				} else if (world::hasLookText(mouseXTile, mouseYTile)) {
					lookReticle.setTextureRect(sf::IntRect(224, 128, TILESIZE, TILESIZE));
				} else if (world::hasCheckpoint(mouseXTile, mouseYTile)) {
					lookReticle.setTextureRect(sf::IntRect(224, 176, TILESIZE, TILESIZE));
				} else {
					lookReticle.setTextureRect(sf::IntRect(0, TILESIZE, TILESIZE, TILESIZE));
				}
				buttons::setTargetLocation(mouseXTile, mouseYTile);
				windowTexture.draw(lookReticle);
			}

			if(tooltipText.length() > 0){
				//sf::Text tooltip(tooltipText, font, 20);
				////tooltip.setOutlineColor(sf::Color::Black);
				//tooltip.setFillColor(sf::Color::Black);
				//tooltip.setPosition(mouseX - (int)(tooltip.getLocalBounds().width), mouseY - (int)(tooltip.getLocalBounds().height));
				//windowTexture.draw(tooltip); // TODO background for it
				
				int tooltipX = mouseX - getEscapedWidth(tooltipText)*CHARWIDTH;
				if(tooltipX < 0) tooltipX = 0;
				
				drawText(tooltipX, mouseY - CHARHEIGHT, tooltipText);
			
			}
		}
		
		cursorSprite.setPosition(mouseX, mouseY);
		windowTexture.draw(cursorSprite);
		
		windowTexture.display();
		windowSprite.setTexture(windowTexture.getTexture());
		
		window.clear(sf::Color(0,0,0));
		window.draw(windowSprite);
		window.display();
	}

	void cleanup(){
		delete tilesetTexture;
	}
}

#endif
