#include "items.h"
#include "characters.h"
#include "display.h"
//#include "weapons.h"
#include "world.h"
//#include "status.h"

#include <fstream>
#include <sstream>

#include <iostream>
using namespace std;

namespace buttons {
	void enableTargetsForSpell(std::shared_ptr<spell>);
}

#define COIN_VARIANT_COUNT 4

namespace items {
	
	std::vector<item*> allItems;
	item* getItem(int id){
		return allItems[id];
	}

	sf::Texture* itemsTexture;
	sf::Texture* coinsTexture;
	
	sf::Sprite coinSprites[COIN_TYPE_COUNT * COIN_VARIANT_COUNT];

	itemEffect stringToEffect(std::string s){
		stringstream ss;
		ss.str(s);
		itemEffect output;
		char type;
		ss >> type;
		int params;
		switch(type){
			case '-': output.effect = ieDefaultEffect; output.params = 0; return output;
			case 'h': params = 2; output.effect = ieHeal; break;
			case 's': params = 2; output.effect = ieSpirit; break;
			case 'c': params = 1; output.effect = ieCure; break;
			case 'd': params = 2; output.effect = ieDamage; break;
			case 'D': params = 2; output.effect = ieReusableDamage; break;
			case 'e': params = 2; output.effect = ieStatusEffect; break;
			case 't': params = 1; output.effect = ieTerrain; break;
			case 'a': params = 2; output.effect = ieAtmosphere; break;
			case 'S': params = 1; output.effect = ieSpellLike; break;
			case '?': params = 1; output.effect = ieMisc; break;
			case 'E': {
				output.effect = ieEquipment;
				output.params = new int[1];
				return output; // Fill the parameter with object id
			}
			case 'm': params = 2; output.effect = ieMisc; break;
			default: std::cout << "String [" << s << "] results in unknown type " << type << std::endl;
		}
		
		output.params = new int[params];
		for(int i=0; i<params;++i) ss >> output.params[i];
		return output;
	}

	void init(){
		itemsTexture = new sf::Texture;
		itemsTexture->loadFromFile("img/all_items.png");

		coinsTexture = new sf::Texture;
		coinsTexture->loadFromFile("img/all_coins.png");
		
		int tilesetWidth = itemsTexture->getSize().x / ITEM_IMGSIZE;
		//int tilesetHeight = itemsTexture->getSize().y / ITEM_IMGSIZE;
		
		//Adhoc Serialized Data Format
		ifstream itemFile("misc/items.asdf");
		string line;
		stringstream lineStream;
		int id = 0;
		while(getline(itemFile, line)){
			lineStream.str(line);
			lineStream.clear();
			// Line format is {name}\t{flavor}\t{category}\t{useEffect}\t{throwEffect}
			
			item* i = new item(id);
			
			getline(lineStream, i->name, '\t');
			getline(lineStream, i->flavor, '\t');
			string temp;
			
			getline(lineStream, temp, '\t');
			i->category = stoi(temp);
			
			getline(lineStream, temp, '\t');
			i->useEffect = stringToEffect(temp);
			if(i->useEffect.effect == ieEquipment) i->useEffect.params[0] = id;
			// This is easier so that useItemOnCharacter can still take effects instead of items
			
			getline(lineStream, temp, '\t');
			i->throwEffect = stringToEffect(temp);
			
			i->sprite = new sf::Sprite;
			i->sprite->setTexture(*itemsTexture);
			i->sprite->setTextureRect(sf::IntRect(id%tilesetWidth * ITEM_IMGSIZE, id/tilesetWidth * ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE));
			
			allItems.push_back(i);
			++id;
		}

		for (int i=0; i < COIN_TYPE_COUNT * COIN_VARIANT_COUNT; ++i) {
			coinSprites[i].setTexture(*coinsTexture);
			coinSprites[i].setTextureRect(sf::IntRect(ITEM_IMGSIZE * (i % COIN_VARIANT_COUNT), ITEM_IMGSIZE * (i / COIN_VARIANT_COUNT), ITEM_IMGSIZE, ITEM_IMGSIZE));
		}
	}

	bool useItemOnCharacter(itemEffect ie, StattedCharacter* c) {
		// Returns true if the item is consumed
		
		PlayerCharacter* pc = dynamic_cast<PlayerCharacter*>(c);
		
		switch(ie.effect){
			case ieHeal:
				std::cout << "item is heal" << std::endl;
				if(ie.params[1]){
					if(pc) display::sparkleHealthMeter(pc);
					c->takeDamage(ie.params[0], damage_type::holy);
				} else {
					// animation on sprite of healing
					c->heal(ie.params[0]);
				}
				return true;
			case ieSpirit:
				std::cout << "item is spirit" << std::endl;
				if(!pc) return false;
				if(ie.params[1]) pc->setSpirit(pc->getSpirit() + ie.params[0]);
				else if(pc->getSpirit() + ie.params[0] > pc->getMaxSpirit()){
					pc->setSpirit(pc->getMaxSpirit());
				} else {
					pc->setSpirit(pc->getSpirit() + ie.params[0]);
				}
				display::sparkleSpiritMeter(pc);
				return true;
			case ieDamage:
			case ieReusableDamage:
				std::cout << "item is damage" << std::endl;
				c->takeDamage(ie.params[1], (damage_type)ie.params[0]);
				return ie.effect == ieDamage;
			// case cure: choose certain status conditions based on params and remove them
			case ieStatusEffect:
				std::cout << "item is condition" << std::endl;
				c->addCondition((conditionId)ie.params[0], ie.params[1]);
				return true;
			// case terrain: apply terrain effect params[0] in square of radius params[1] centered on character's xy
			// case atmosphere: apply weather effect params[0] for duration params[1]
			case ieSpellLike: {
				/*std::cout << "item is spell-like" << std::endl;
				if(!pc) return false;
				std::shared_ptr<spell> sp = spells::get(ie.params[0]);
				if(pc->canSpendSpirit(sp->cost)) {
					buttons::enableTargetsForSpell(sp);
				}*/
				return false;
			}
			case ieEquipment:
				if(!pc) return false;
				return pc->equip(ie.params[0]);
			case ieMisc:
				std::cout << "item is other" << std::endl;
				switch(ie.params[0]){
					case 0: { // water bubble
						c->removeCondition(burning);
						world::onWaterHit(c->getX(), c->getY());
						return true;
					}
				}
				break;
			default:
				std::cout << "item is...? " << ie.effect << std::endl;
		}
		return false;
	}

	int itemCategoryToSpriteIndex(int category) {
		switch(category) {
			case 1: return 2; // bottle
			case 2: return 3; // plant
			case 4: return 5; // counterweight
			case 5: return 6; // key
			case 7: return 7; // bubble
			default: return 1; // default item
		}
	}

	void throwItemAtLocation(item* i, int x, int y) {
		if(StattedCharacter* c = world::getCharacterAt(x,y)){
			if(useItemOnCharacter(i->throwEffect, c)) return;
		}
		switch(i->throwEffect.effect){
			case ieDamage:
			case ieReusableDamage:
				// damageAtTile will show the damage animation and then call damageTerrain.
				// this does result in a redundant getCharacterAt though
				world::damageAtTile(x, y, i->throwEffect.params[1], (damage_type)i->throwEffect.params[0]);
				world::putItem(x,y,i);
				break;
			// spellLike and atmosphere won't be on throw effects
			// If there's no character: heal, spirit, statusEffect, and cure won't do anything
			// (probably), and damage will have very limited uses
			// case terrain:
			case ieTerrain:
				world::setTransientTerrain(x, y, i->throwEffect.params[0]);
				break;
			case ieMisc:
				switch(i->throwEffect.params[0]){
					case 0: { // water bubble
						world::onWaterHit(x, y);
						break;
					}
				}
				break;
			default: world::putItem(x,y,i);
		}
	}
}

bool item::isPermanent() const {
	return this->useEffect.effect == ieEquipment
	    || this->useEffect.effect == ieSpellLike
		|| this->throwEffect.effect == ieReusableDamage
		;
}

coinPile::coinPile(coinType _ct, int _count) : type(_ct), count(_count) {
	int variantIndex = 0;
	if(count >= 10) variantIndex = 3;
	else if(count >= 5) variantIndex = 2;
	else if(count > 1) variantIndex = 1;
	sprite = &(items::coinSprites[variantIndex + type*COIN_VARIANT_COUNT]);
	std::cout << "Variant index: " << variantIndex << "; type: " << type << "; COIN_VARIANT_COUNT: " << COIN_VARIANT_COUNT << "; sprite index: " << (variantIndex + type*COIN_VARIANT_COUNT) << std::endl;
}

