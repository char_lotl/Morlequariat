#include <fstream>
#include <sstream>

#include "spells.h"
#include "world.h"
#include "characters.h"

using namespace std;

namespace spells {

	std::vector<std::shared_ptr<spell>> allSpells;

	void init() {
	
		ifstream spellFile("misc/spells.asdf");
		string line;
		stringstream lineStream;
		int id = 0;
		while(getline(spellFile, line)){
			lineStream.str(line);
			lineStream.clear();
			
			std::shared_ptr<spell> i = std::shared_ptr<spell> { new spell };
			i->id = id;
			getline(lineStream, i->name, '\t');
			
			string temp;
			getline(lineStream, temp, '\t');
			i->cost = stoi(temp);
			
			getline(lineStream, temp, '\t');
			i->rangeMod = stoi(temp);
			
			getline(lineStream, temp, '\t');
			i->rtype = (range_type)stoi(temp);
			
			getline(lineStream, i->description, '\t');
			
			allSpells.push_back(i);
			++id;
		}
	}
	
	std::shared_ptr<spell> get(int i) { return allSpells[i]; }

	std::vector<std::pair<int, int> > getTargets(std::shared_ptr<spell> s, PlayerCharacter* caster) {
		int totalRange = s->rangeMod + caster->getRange();
		if(s->rtype == range_type::custom) {
			
			std::vector<std::pair<int, int> > targets;
			
			switch(s->id){
				case 0: {
					for(int i=1; i<=totalRange; ++i) {
						if(world::blocked(caster->getX()+i, caster->getY()) || world::blockedOneWay(caster->getX(), caster->getY(), caster->getX()+i, caster->getY())) break;
						if(caster->canMoveTo(caster->getX()+i, caster->getY())) targets.push_back(std::make_pair(i, 0));
					}
					for(int i=1; i<=totalRange; ++i) {
						if(world::blocked(caster->getX()-i, caster->getY()) || world::blockedOneWay(caster->getX(), caster->getY(), caster->getX()-i, caster->getY())) break;
						if(caster->canMoveTo(caster->getX()-i, caster->getY())) targets.push_back(std::make_pair(-i, 0));
					}
					for(int i=1; i<=totalRange; ++i) {
						if(world::blocked(caster->getX(), caster->getY()+i) || world::blockedOneWay(caster->getX(), caster->getY(), caster->getX(), caster->getY()+i)) break;
						if(caster->canMoveTo(caster->getX(), caster->getY()+i)) targets.push_back(std::make_pair(0, i));
					}
					for(int i=1; i<=totalRange; ++i) {
						if(world::blocked(caster->getX(), caster->getY()-i) || world::blockedOneWay(caster->getX(), caster->getY(), caster->getX(), caster->getY()-i)) break;
						if(caster->canMoveTo(caster->getX(), caster->getY()-i)) targets.push_back(std::make_pair(0, -i));
					}
					break;
				}
			}
			
			return targets;
		} else {
			return weapons::getTargets(s->rtype, totalRange);
		}
	}
	
	int getAreaRadius(std::shared_ptr<spell> s, PlayerCharacter* caster) {
		// only valid for "self" spells
		return s->rangeMod + caster->getRange(); // in the future, some may be always 0
	}
	
}
